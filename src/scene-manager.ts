'use strict';

import { CONFIG } from './config';
import BootScene from './scenes/boot/boot-scene';
import CreditsScene from './scenes/credits/credits-scene';
import GameOverScene from './scenes/game-over/game-over-scene';
import GameScene from './scenes/game/game-scene';
import LoadingScene from './scenes/loading/loading-scene';
import PauseScene from './scenes/pause/pause-scene';
import TitleScene from './scenes/title/title-scene';
import Facade from './services/facade/facade';
import SCENE_EVENTS from './services/subject/events/scene-events';

export default class SceneManager {
  constructor(
    private game: Phaser.Game,
    private facade: Facade
  ) {
    this.initScenes();
    this.initListeners();
  }

  start(): void {
    this.startScene(CONFIG.scenes.BOOT);
  }

  private initScenes(): void {
    const bootScene: BootScene = new BootScene(this.facade);
    this.game.scene.add(CONFIG.scenes.BOOT, bootScene);

    const loadingScene: LoadingScene = new LoadingScene(this.facade);
    this.game.scene.add(CONFIG.scenes.LOADING, loadingScene);

    const titleScene: TitleScene = new TitleScene(this.facade);
    this.game.scene.add(CONFIG.scenes.TITLE, titleScene);

    const gameScene: GameScene = new GameScene(this.facade);
    this.game.scene.add(CONFIG.scenes.GAME, gameScene);

    const pauseScene: PauseScene = new PauseScene(this.facade);
    this.game.scene.add(CONFIG.scenes.PAUSE, pauseScene);

    const gameOverScene: GameOverScene = new GameOverScene(this.facade);
    this.game.scene.add(CONFIG.scenes.GAME_OVER, gameOverScene);

    const creditsScene: CreditsScene = new CreditsScene(this.facade);
    this.game.scene.add(CONFIG.scenes.CREDITS, creditsScene);
  }

  private initListeners(): void {
    this.facade.subjectService.subscribe(SCENE_EVENTS.START_LOADING,
      (previousScene, data) => this.startScene(CONFIG.scenes.LOADING, previousScene, data));
    this.facade.subjectService.subscribe(SCENE_EVENTS.START_TITLE,
      (previousScene, data) => this.startScene(CONFIG.scenes.TITLE, previousScene, data));
    this.facade.subjectService.subscribe(SCENE_EVENTS.START_GAME,
      (previousScene, data) => this.startScene(CONFIG.scenes.GAME, previousScene, data));
    this.facade.subjectService.subscribe(SCENE_EVENTS.START_GAME_OVER,
      (previousScene, data) => this.startScene(CONFIG.scenes.GAME_OVER, previousScene, data));
    this.facade.subjectService.subscribe(SCENE_EVENTS.START_CREDITS,
      (previousScene, data) => this.startScene(CONFIG.scenes.CREDITS, previousScene, data));
  }

  private startScene(sceneKey: string, previousSceneKey?: string, data?: any): void {
    this.facade.subjectService.dispatch(SCENE_EVENTS.SCENE_CHANGED, sceneKey, previousSceneKey);
    if (previousSceneKey !== undefined) {
      this.game.scene.stop(previousSceneKey);
    }
    this.facade.entityManager.cleanup();
    this.facade.subjectService.clearSubscribersForAllEvents();
    this.initListeners();
    this.game.scene.start(sceneKey, data);
  }
}
