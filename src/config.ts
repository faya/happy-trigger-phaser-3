'use strict';

export const CONFIG: IGlobalConfig = {
  scenes: {
    BOOT: 'SCENE:BOOT',
    LOADING: 'SCENE:LOADING',
    TITLE: 'SCENE:TITLE',
    GAME: 'SCENE:GAME',
    PAUSE: 'SCENE:PAUSE',
    GAME_OVER: 'SCENE:GAME_OVER',
    CREDITS: 'SCENE:CREDITS'
  },
  game: {
    IS_MOBILE: false,
    GRAVITY: 3000,
    HERO_MAX_HEALTH: 10,
    HERO_VELOCITY: 400,
    HERO_PROJECTILE_VELOCITY: 1600,
    HERO_FIRE_RATE: 250,
    HERO_BASE_DAMAGE: 2,
    KNOCKBACK: 30,
    EXPLOSION_PROBABILITY: 0.2,
    ENNEMY_KILLED_BONUS: 100,
    ENNEMY_COLLISION_DAMAGE: 1,
    BONUS_PROBABILITY: 0.2
  },
  size: {
    GAME_WIDTH: 1200,
    GAME_ASPECT_RATIO: 4 / 3,
    get GAME_HEIGHT() {
      return this.GAME_WIDTH / this.GAME_ASPECT_RATIO;
    },
    TILE_SIZE: 60,
    BASE_FONT_SIZE: 40,
    HERO_WIDTH: 83,
    HERO_HEIGHT: 120,
    HERO_ARM_LENGTH: 60,
    CROSSHAIR_DISTANCE: 300,
    SCALE: 1.0,
    get SCALED_GAME_WIDTH() {
      return this.GAME_WIDTH * this.SCALE;
    },
    get SCALED_GAME_HEIGHT() {
      return this.GAME_HEIGHT * this.SCALE;
    },
    get SCALED_TILE_SIZE() {
      return this.TILE_SIZE * this.SCALE;
    },
    get SCALED_HERO_ARM_LENGTH() {
      return this.HERO_ARM_LENGTH * this.SCALE;
    }
  },
  options: {
    MUSIC_VOLUME: 0.3
  }
};

export default interface IGlobalConfig {
  game: IGameConfig;
  size: ISizeConfig;
  options: IOptionsConfig;
  scenes: { [key: string]: string; };
}

export interface IGameConfig {
  IS_MOBILE: boolean;
  GRAVITY: number;
  HERO_MAX_HEALTH: number;
  HERO_VELOCITY: number;
  HERO_PROJECTILE_VELOCITY: number;
  HERO_FIRE_RATE: number;
  HERO_BASE_DAMAGE: number;
  KNOCKBACK: number;
  EXPLOSION_PROBABILITY: number;
  ENNEMY_KILLED_BONUS: number;
  ENNEMY_COLLISION_DAMAGE: number;
  BONUS_PROBABILITY: number;
}

export interface ISizeConfig {
  GAME_WIDTH: number;
  GAME_ASPECT_RATIO: number;
  GAME_HEIGHT: number;
  BASE_FONT_SIZE: number;
  TILE_SIZE: number;
  HERO_WIDTH: number;
  HERO_HEIGHT: number;
  HERO_ARM_LENGTH: number;
  SCALE: number;
  SCALED_GAME_WIDTH: number;
  SCALED_GAME_HEIGHT: number;
  SCALED_TILE_SIZE: number;
  SCALED_HERO_ARM_LENGTH: number;
  CROSSHAIR_DISTANCE: number;
}

export interface IOptionsConfig {
  MUSIC_VOLUME: number;
}
