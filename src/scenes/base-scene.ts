'use strict';

import System from '../services/ecs/systems/system';
import Facade from '../services/facade/facade';
import BaseController from './base-controller';

export default abstract class BaseScene extends Phaser.Scene {
  protected controllers: BaseController[] = [];
  protected systems: System[] = [];
  protected shouldUpdate: boolean = true;

  constructor(
    sceneConfig: string | Phaser.Scenes.Settings.Config,
    private facade: Facade
  ) {
    super(Object.assign(sceneConfig, { active: false }));
  }

  protected get F(): Facade {
    return this.facade;
  }

  registerController(controller: BaseController): void {
    this.controllers.push(controller);
  }

  registerSystem(system: System): void {
    this.systems.push(system);
  }

  init(): void {
    this.controllers = [];
    this.systems = [];
  }

  preload(): void {
    this.controllers.forEach(controller => controller.preload());
    this.systems.forEach(system => system.preload());
  }

  create(): void {
    this.controllers.forEach(controller => controller.create());
    this.systems.forEach(system => system.create());
  }

  update(time: number, delta: number): void {
    if (!this.scene.isActive(this.scene.key)) {
      return;
    }
    this.systems.forEach((system) => {
      if (this.shouldUpdate) system.update(time, delta);
    });
    this.controllers.forEach((controller) => {
      if (this.shouldUpdate) controller.update();
    });
  }

  render(): void {
    if (!this.scene.isActive(this.scene.key)) {
      return;
    }
    this.controllers.forEach(controller => controller.render());
    this.systems.forEach(system => system.render());
  }

  protected stopControllersAndSystems(): void {
    this.shouldUpdate = false;
  }
}
