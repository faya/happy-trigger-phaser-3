'use strict';

import BitmapText from '../../game-objects/components/bitmap-text';
import Position from '../../game-objects/components/position';
import BitmapTextSystem from '../../game-objects/systems/bitmap-text-system';
import Entity from '../../services/ecs/entities/entity';
import Facade from '../../services/facade/facade';
import GAME_EVENTS from '../../services/subject/events/game-events';
import BaseScene from '../base-scene';

export default class PauseScene extends BaseScene {
  private pauseEntities: Entity[] = [];
  private echapKey: Phaser.Input.Keyboard.Key;

  constructor(facade: Facade) {
    super({
      key: facade.config.scenes.PAUSE
    }, facade);
  }

  init(): void {
    this.registerSystem(new BitmapTextSystem(this, this.F));
  }

  preload(): void {
    this.F.entityManager.createCollection('pause');
  }

  create(): void {
    this.createPauseMenu();
    this.input.once('pointerup', this.resumeGame.bind(this));
    this.echapKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ESC);
    super.create();
  }

  update(time: number, delta: number): void {
    super.update(time, delta);
    if (Phaser.Input.Keyboard.JustDown(this.echapKey)) {
      this.resumeGame();
    }
  }

  private createPauseMenu(): void {
    const title = this.F.entityManager.createEntity('pause');
    this.F.entityManager.addComponentToEntity(title, BitmapText, {
      font: 'gameFont',
      text: 'Paused',
      size: this.F.config.size.BASE_FONT_SIZE * 3
    });
    this.F.entityManager.addComponentToEntity(title, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH / 2,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 4
    });

    const resume = this.F.entityManager.createEntity('pause');
    this.F.entityManager.addComponentToEntity(resume, BitmapText, {
      font: 'gameFont',
      text: 'click to resume',
      size: this.F.config.size.BASE_FONT_SIZE * 2
    });
    this.F.entityManager.addComponentToEntity(resume, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH / 2,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2
    });

    this.pauseEntities.push(title);
    this.pauseEntities.push(resume);
  }

  private resumeGame(): void {
    this.F.entityManager.deleteCollection('pause');
    this.scene.stop(this.scene.key);
    this.scene.resume(this.F.config.scenes.GAME);
    this.F.subjectService.dispatch(GAME_EVENTS.RESUMED);
  }
}
