'use strict';

import GameObject from '../game-objects/game-object';

export default abstract class BaseController extends GameObject {}
