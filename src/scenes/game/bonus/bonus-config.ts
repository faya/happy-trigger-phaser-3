'use strict';

import { EFFECT_KEYS } from '../../../game-objects/effects';

export const BONUS_CONFIG = {
  [EFFECT_KEYS.HEALTH_BONUS]: {
    sprite: 'heart',
    animation: { key: 'shine', frameConfig: { start: 0, end: 7 } },
    options: { bonusValue: 1 }
  },
  [EFFECT_KEYS.SCORE_BONUS]: {
    sprite: 'coin',
    animation: { key: 'shine', frameConfig: { start: 0, end: 9 } },
    options: { bonusValue: 200 }
  }
};

export const BONUS_POOL = [EFFECT_KEYS.HEALTH_BONUS, EFFECT_KEYS.SCORE_BONUS];
