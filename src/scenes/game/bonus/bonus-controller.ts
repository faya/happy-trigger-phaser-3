'use strict';

import ArcadeSprite from '../../../game-objects/components/arcade-sprite';
import CollectableEffect from '../../../game-objects/components/collectable-effect';
import Collidable from '../../../game-objects/components/collidable';
import Position from '../../../game-objects/components/position';
import Sprite from '../../../game-objects/components/sprite';
import SpriteAnimation from '../../../game-objects/components/sprite-animation';
import State from '../../../game-objects/components/state';
import Entity from '../../../services/ecs/entities/entity';
import WAVE_EVENTS from '../../../services/subject/events/wave-events';
import BaseController from '../../base-controller';
import { BONUS_CONFIG, BONUS_POOL } from './bonus-config';

export default class BonusController extends BaseController {
  preload(): void {
    this.F.entityManager.createCollection('bonus');
    this.F.subjectService.subscribe(WAVE_EVENTS.ENNEMY_DIED, this.trySpawnBonus.bind(this));
  }

  private trySpawnBonus(deadEnnemy: Entity): void {
    if (Math.random() >= this.F.config.game.BONUS_PROBABILITY) {
      return;
    }

    // choose a random bonus
    const bonusIndex = Math.floor(Math.random() * BONUS_POOL.length);
    const bonusKey = BONUS_POOL[bonusIndex];

    const bonus = this.F.entityManager.createEntity('bonus');
    const { x, y } = this.F.entityManager.getComponentFromEntity(deadEnnemy, Sprite).state.sprite.body;
    this.F.entityManager.addComponentToEntity(bonus, Position, { x, y });
    this.F.entityManager.addComponentToEntity(bonus, Sprite, {
      key: BONUS_CONFIG[bonusKey].sprite,
      animations: [[BONUS_CONFIG[bonusKey].animation.key, BONUS_CONFIG[bonusKey].animation.frameConfig, 10, true]]
    });
    this.F.entityManager.addComponentToEntity(bonus, SpriteAnimation, {
      componentType: Sprite,
      stateSpriteKey: 'sprite',
      current: `${bonus.id}|${BONUS_CONFIG[bonusKey].animation.key}`,
      repeat: true
    });
    this.F.entityManager.addComponentToEntity(bonus, ArcadeSprite, { collidesWorldBounds: true });
    this.F.entityManager.addComponentToEntity(bonus, State,
      { componentType: Sprite, stateComponentKey: 'sprite', jumpCharges: 0 });
    this.F.entityManager.addComponentToEntity(bonus, Collidable, {
      componentType: Sprite,
      stateCollidableKey: 'sprite',
      collidesWith: [{
        filters: { tags: ['platform'] },
        componentType: Sprite,
        stateCollidableKey: 'sprite'
      }]
    });
    this.F.entityManager.addComponentToEntity(bonus, CollectableEffect, {
      effectKey: bonusKey,
      effectData: BONUS_CONFIG[bonusKey].options,
      canCollectFilters: { collectionNames: ['hero'] },
      destroyOnCollect: true
    });
  }
}
