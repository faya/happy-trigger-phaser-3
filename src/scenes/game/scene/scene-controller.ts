'use strict';

import ArcadeSprite from '../../../game-objects/components/arcade-sprite';
import Position from '../../../game-objects/components/position';
import Sprite from '../../../game-objects/components/sprite';
import TILES_DATA from '../../../game-objects/tiles/tiles-data';
import Entity from '../../../services/ecs/entities/entity';
import BaseController from '../../base-controller';
import BackgroundFactory from '../../common/background-factory';

export default class SceneController extends BaseController {
  preload(): void {
    this.F.entityManager.createCollection('background');
    this.F.entityManager.createCollection('tiles');
  }

  create(): void {
    new BackgroundFactory(this.F).build('background');
    this.createTiles();
  }

  private createTiles(): void {
    const tileSize: number = this.F.config.size.TILE_SIZE * this.F.config.size.SCALE;
    for (let xx = 0; xx < TILES_DATA[0].length; xx++) {
      for (let yy = 0; yy < TILES_DATA.length; yy++) {
        if (TILES_DATA[yy][xx] !== 0) {
          const tileEntity: Entity = this.F.entityManager.createEntity('tiles', ['platform']);
          this.F.entityManager.addComponentToEntity(tileEntity, Sprite, { key: 'tile', origin: [0, 0] });
          this.F.entityManager.addComponentToEntity(tileEntity, Position, { x: xx * tileSize, y: yy * tileSize });
          this.F.entityManager.addComponentToEntity(tileEntity, ArcadeSprite, { immovable: true });
        }
      }
    }
  }
}
