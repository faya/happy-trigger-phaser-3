'use strict';

import Spawn from '../../../game-objects/components/spawn';
import { SpawnData, WaveData } from '../../../game-objects/waves/wave-interfaces';
import WAVE_DATA from '../../../game-objects/waves/waves-data';
import REGISTRY from '../../../registry';
import Entity from '../../../services/ecs/entities/entity';
import SCENE_EVENTS from '../../../services/subject/events/scene-events';
import WAVE_EVENTS from '../../../services/subject/events/wave-events';
import BaseController from '../../base-controller';

export default class WaveController extends BaseController {
  private spawns: Entity[];

  create(): void {
    this.scene.registry.set(REGISTRY.CURRENT_WAVE, 0);
    this.scene.registry.set(REGISTRY.NB_REMAINING_ENNEMIES, 0);
    this.F.subjectService.subscribe(WAVE_EVENTS.ENNEMY_DIED, this.checkForWaveEnd.bind(this));
    this.F.subjectService.subscribe(WAVE_EVENTS.WAVE_END, this.startNextWave.bind(this));
    this.startNextWave();
  }

  private get currentWaveData(): WaveData {
    return WAVE_DATA[this.scene.registry.get(REGISTRY.CURRENT_WAVE)];
  }

  private checkForWaveEnd(): void {
    const finishedSpawning = this.spawns
      .map((s) => {
        const spawn = this.F.entityManager.getComponentFromEntity(s, Spawn);
        return spawn.state.ennemyPool !== undefined && spawn.state.ennemyPool.length === 0;
      })
      .reduce((curr, agg) => curr && agg);

    if (this.scene.registry.get(REGISTRY.NB_REMAINING_ENNEMIES) <= 0 && finishedSpawning) {
      this.F.entityManager.deleteCollection('spawn');
      const nextWave = this.scene.registry.get(REGISTRY.CURRENT_WAVE) + 1;
      this.scene.registry.set(REGISTRY.CURRENT_WAVE, nextWave);
      this.F.subjectService.delayedDispatch(WAVE_EVENTS.WAVE_END, 1000, nextWave - 1);
    }
  }

  private startNextWave(): void {
    this.F.entityManager.createCollection('spawn');
    this.spawns = [];

    const waveData = this.currentWaveData;
    if (waveData === undefined) {
      this.F.subjectService.dispatch(WAVE_EVENTS.LAST_WAVE_END);
      return;
    }

    this.F.subjectService.dispatch(WAVE_EVENTS.WAVE_START, this.scene.registry.get(REGISTRY.CURRENT_WAVE));
    // TODO: play music
    setTimeout(() => {
      for (const spawnData of this.currentWaveData.spawns) {
        this.createSpawn(spawnData);
      }
    }, 4000);
  }

  private createSpawn(data: SpawnData): void {
    const spawn = this.F.entityManager.createEntity('spawn');
    this.F.entityManager.addComponentToEntity(spawn, Spawn, { config: data });
    this.spawns.push(spawn);
  }
}
