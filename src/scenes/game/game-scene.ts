'use strict';

import ArcadeSpriteSystem from '../../game-objects/systems/arcade-sprite-system';
import AudioSystem from '../../game-objects/systems/audio-system';
import BehaviorSystem from '../../game-objects/systems/behavior-system';
import BitmapTextSystem from '../../game-objects/systems/bitmap-text-system';
import CollectableEffectSystem from '../../game-objects/systems/collectable-effect-system';
import CollidableSystem from '../../game-objects/systems/collidable-system';
import ControllableByEventsSystem from '../../game-objects/systems/controllable-by-events-system';
import ControllableByMatrixSystem from '../../game-objects/systems/controllable-by-matrix-system';
import DamageOverlapingSystem from '../../game-objects/systems/damage-overlaping-system';
import EventOnClickSystem from '../../game-objects/systems/event-on-click-system';
import ExplosionSystem from '../../game-objects/systems/explosion-system';
import FlashOnDamageSystem from '../../game-objects/systems/flash-on-damage-system';
import HealthSystem from '../../game-objects/systems/health-system';
import HeroSpriteSystem from '../../game-objects/systems/hero-sprite-system';
import KnockBackOnDamageSystem from '../../game-objects/systems/knockback-on-damage-system';
import ProjectileSystem from '../../game-objects/systems/projectile-system';
import RegularTextSystem from '../../game-objects/systems/regular-text-system';
import ShootingSystem from '../../game-objects/systems/shooting-system';
import SpawnSystem from '../../game-objects/systems/spawn-system';
import SpriteAnimationSystem from '../../game-objects/systems/sprite-animation-system';
import SpriteSystem from '../../game-objects/systems/sprite-system';
import StateAnimationControlSystem from '../../game-objects/systems/state-animation-control-system';
import StateSystem from '../../game-objects/systems/state-system';
import TweenSystem from '../../game-objects/systems/tween-system';
import REGISTRY from '../../registry';
import Facade from '../../services/facade/facade';
import GAME_EVENTS from '../../services/subject/events/game-events';
import HERO_EVENTS from '../../services/subject/events/hero-events';
import SCENE_EVENTS from '../../services/subject/events/scene-events';
import WAVE_EVENTS from '../../services/subject/events/wave-events';
import { EVENT_PRIORITIES } from '../../services/subject/subject-service';
import BaseScene from '../base-scene';
import AudioController from './audio/audio-controller';
import BonusController from './bonus/bonus-controller';
import EnnemyController from './ennemy/ennemy-controller';
import HeroController from './hero/hero-controller';
import HudController from './hud/hud-controller';
import DesktopInputController from './input/desktop-input-controller';
import MobileInputController from './input/mobile-input-controller';
import SceneController from './scene/scene-controller';
import WaveController from './wave/wave-controller';

export default class GameScene extends BaseScene {
  private isGamePaused: boolean = false;

  constructor(facade: Facade) {
    super({
      key: facade.config.scenes.GAME
    }, facade);
  }

  init(): void {
    super.init();

    this.registerController(new SceneController(this, this.F));
    this.registerController(new EnnemyController(this, this.F));
    this.registerController(new HeroController(this, this.F));
    this.registerController(new HudController(this, this.F));
    this.registerController(new WaveController(this, this.F));
    this.registerController(new BonusController(this, this.F));
    this.registerController(new AudioController(this, this.F));
    if (this.F.config.game.IS_MOBILE) {
      this.registerController(new MobileInputController(this, this.F));
    }
    else {
      this.registerController(new DesktopInputController(this, this.F));
    }

    this.registerSystem(new HealthSystem(this, this.F));
    this.registerSystem(new SpriteSystem(this, this.F));
    this.registerSystem(new ArcadeSpriteSystem(this, this.F));
    this.registerSystem(new HeroSpriteSystem(this, this.F));
    this.registerSystem(new BitmapTextSystem(this, this.F));
    this.registerSystem(new RegularTextSystem(this, this.F));
    this.registerSystem(new ShootingSystem(this, this.F));
    this.registerSystem(new ProjectileSystem(this, this.F));
    this.registerSystem(new StateSystem(this, this.F));
    this.registerSystem(new CollidableSystem(this, this.F));
    this.registerSystem(new ControllableByEventsSystem(this, this.F));
    this.registerSystem(new StateAnimationControlSystem(this, this.F));
    this.registerSystem(new SpriteAnimationSystem(this, this.F));
    this.registerSystem(new ControllableByMatrixSystem(this, this.F));
    this.registerSystem(new BehaviorSystem(this, this.F));
    this.registerSystem(new TweenSystem(this, this.F));
    this.registerSystem(new ExplosionSystem(this, this.F));
    this.registerSystem(new KnockBackOnDamageSystem(this, this.F));
    this.registerSystem(new FlashOnDamageSystem(this, this.F));
    this.registerSystem(new DamageOverlapingSystem(this, this.F));
    this.registerSystem(new CollectableEffectSystem(this, this.F));
    this.registerSystem(new SpawnSystem(this, this.F));
    this.registerSystem(new AudioSystem(this, this.F));
    this.registerSystem(new EventOnClickSystem(this, this.F));
  }

  preload(): void {
    super.preload();
    this.F.subjectService.subscribe(GAME_EVENTS.PAUSED, this.pauseGame.bind(this));
    this.F.subjectService.subscribe(HERO_EVENTS.DIED, this.gameOver.bind(this), EVENT_PRIORITIES.HIGH);
    this.F.subjectService.subscribe(WAVE_EVENTS.LAST_WAVE_END, this.endGame.bind(this));
  }

  create(): void {
    this.physics.world.setBounds(0, 0, this.F.config.size.SCALED_GAME_WIDTH, this.F.config.size.SCALED_GAME_HEIGHT);
    this.registry.set(REGISTRY.SCORE, 0);
    this.isGamePaused = false;
    this.shouldUpdate = true;
    this.events.on('resume', () => this.isGamePaused = false);
    if (this.F.config.game.IS_MOBILE) {
      this.input.addPointer();
    }
    super.create();
  }

  private pauseGame(): void {
    if (this.isGamePaused) {
      return;
    }
    this.isGamePaused = true;
    this.scene.pause();
    this.scene.launch(this.F.config.scenes.PAUSE);
  }

  private gameOver(): void {
    this.stopControllersAndSystems();
    this.updateHighScore();
    const data: any = { score: this.registry.get(REGISTRY.SCORE) };
    this.F.subjectService.delayedDispatch(SCENE_EVENTS.START_GAME_OVER, 3500, this.scene.key, data);
  }

  private endGame(): void {
    this.stopControllersAndSystems();
    this.updateHighScore();
    const data: any = { score: this.registry.get(REGISTRY.SCORE) };
    this.F.subjectService.delayedDispatch(SCENE_EVENTS.START_CREDITS, 1500, this.scene.key, data);
  }

  private updateHighScore(): void {
    if (!localStorage['highscore'] || localStorage['highscore'] <= this.registry.get(REGISTRY.SCORE)) {
      localStorage['highscore'] = this.registry.get(REGISTRY.SCORE);
    }
  }
}
