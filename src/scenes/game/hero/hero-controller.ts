'use strict';

import Entity from '../../../services/ecs/entities/entity';
import HERO_EVENTS from '../../../services/subject/events/hero-events';
import BaseController from '../../base-controller';
import HeroFactory from './hero-factory';

export default class HeroController extends BaseController {
  private hero: Entity;

  preload(): void {
    this.F.entityManager.createCollection('hero');
    this.F.subjectService.subscribe(HERO_EVENTS.DIED, this.cleanupHeroEntity.bind(this));
  }

  create(): void {
    this.hero = new HeroFactory(this.F).build();
  }

  private cleanupHeroEntity(): void {
    this.F.entityManager.destroyEntities({ ids: [this.hero.id] });
  }
}
