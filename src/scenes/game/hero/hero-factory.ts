'use strict';

import Collidable from '../../../game-objects/components/collidable';
import ControllableByEvents from '../../../game-objects/components/controllable-by-events';
import Health from '../../../game-objects/components/health';
import HeroSprite from '../../../game-objects/components/hero-sprite';
import Position from '../../../game-objects/components/position';
import Shooting from '../../../game-objects/components/shooting';
import Sprite from '../../../game-objects/components/sprite';
import SpriteAnimation from '../../../game-objects/components/sprite-animation';
import State from '../../../game-objects/components/state';
import StateAnimationControl from '../../../game-objects/components/state-animation-control';
import TweenQueue from '../../../game-objects/components/tween';
import ENTITY_STATE from '../../../game-objects/misc/entity-state';
import Entity from '../../../services/ecs/entities/entity';
import Facade from '../../../services/facade/facade';
import GAME_EVENTS from '../../../services/subject/events/game-events';
import HERO_EVENTS from '../../../services/subject/events/hero-events';

const HERO_START_POS = { x: 10, y: 6 };
const WEAPON_ANCHOR = { x: 0.2, y: 0.1 };

export default class HeroFactory {
  constructor(
    private F: Facade
  ) {}

  build(): Entity {
    const hero = this.F.entityManager.createEntity('hero', ['hero']);

    const xPos = HERO_START_POS.x * this.F.config.size.SCALED_TILE_SIZE;
    const yPos = HERO_START_POS.y * this.F.config.size.SCALED_TILE_SIZE;

    this.F.entityManager.addComponentToEntity(hero, Health, {
      max: this.F.config.game.HERO_MAX_HEALTH,
      eventOnDeath: HERO_EVENTS.DIED
    });
    this.F.entityManager.addComponentToEntity(hero, Position, { x: xPos, y: yPos });
    this.F.entityManager.addComponentToEntity(hero, HeroSprite, {
      spritesPos: {
        weapon: [this.F.config.size.HERO_WIDTH * WEAPON_ANCHOR.x, this.F.config.size.HERO_HEIGHT * WEAPON_ANCHOR.y],
        body: [0, 0],
        crosshair: [0, 0]
      },
      spritesData: {
        weapon: {
          key: 'hero-weapon-arm',
          origin: [0.075, 0.4],
          angle: 30,
          animations: [
            ['muzzle-flash', { frames: [1, 0] }, 10, false]
          ]
        },
        body: {
          key: 'hero',
          origin: [0.5, 0.5],
          animations: [
            [ENTITY_STATE.IDLE_RIGHT, { frames: [1] }, 10, false],
            [ENTITY_STATE.IDLE_LEFT, { frames: [8] }, 10, false],
            [ENTITY_STATE.RUNNING_RIGHT, { start: 0, end: 4 }, 10, true],
            [ENTITY_STATE.RUNNING_LEFT, { start: 5, end: 9 }, 10, true],
            [ENTITY_STATE.JUMPING_RIGHT, { frames: [0] }, 10, false],
            [ENTITY_STATE.JUMPING_LEFT, { frames: [9] }, 10, false]
          ]
        },
        crosshair: {
          key: 'crosshair'
        }
      }
    });
    this.F.entityManager.addComponentToEntity(hero, SpriteAnimation, {
      componentType: HeroSprite,
      stateSpriteKey: 'bodySprite'
    });
    this.F.entityManager.addComponentToEntity(hero, State, {
      componentType: HeroSprite,
      stateComponentKey: 'container',
      jumpCharges: 2
    });
    this.F.entityManager.addComponentToEntity(hero, ControllableByEvents, {
      componentType: HeroSprite,
      stateBodyKey: 'container',
      leftEvent: HERO_EVENTS.MOVE_LEFT,
      rightEvent: HERO_EVENTS.MOVE_RIGHT,
      jumpEvent: HERO_EVENTS.JUMP,
      horizontalVelocity: this.F.config.game.HERO_VELOCITY,
      jumpVelocity: this.F.config.game.HERO_VELOCITY * 4
    });
    this.F.entityManager.addComponentToEntity(hero, StateAnimationControl, {
      animatedStates: [ENTITY_STATE.IDLE_LEFT, ENTITY_STATE.IDLE_RIGHT, ENTITY_STATE.RUNNING_LEFT,
        ENTITY_STATE.RUNNING_RIGHT, ENTITY_STATE.JUMPING_LEFT, ENTITY_STATE.JUMPING_RIGHT]
    });
    this.F.entityManager.addComponentToEntity(hero, Shooting, {
      targetFilters: { collectionNames: ['ennemy'] },
      origin: [0, 0],
      damage: this.F.config.game.HERO_BASE_DAMAGE,
      angle: 30,
      fireRate: this.F.config.game.HERO_FIRE_RATE,
      projectileSpriteKey: 'projectiles',
      projectileSpriteFrame: 0,
      projectileVelocity: this.F.config.game.HERO_PROJECTILE_VELOCITY,
      onShootEvent: HERO_EVENTS.SHOOT,
      explosionProbability: this.F.config.game.EXPLOSION_PROBABILITY
    });
    this.F.entityManager.addComponentToEntity(hero, Collidable, {
      componentType: HeroSprite,
      stateCollidableKey: 'container',
      collidesWith: [{
        componentType: Sprite,
        stateCollidableKey: 'sprite',
        filters: { tags: ['platform'] },
        collisionEvent: GAME_EVENTS.ENTITY_TOUCHED_PLATFORM
      }]
    });
    this.F.entityManager.addComponentToEntity(hero, TweenQueue, new TweenQueue());

    return hero;
  }
}
