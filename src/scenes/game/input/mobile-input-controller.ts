'use strict';

import HeroSprite from '../../../game-objects/components/hero-sprite';
import Position from '../../../game-objects/components/position';
import Sprite from '../../../game-objects/components/sprite';
import REGISTRY from '../../../registry';
import Entity from '../../../services/ecs/entities/entity';
import HERO_EVENTS from '../../../services/subject/events/hero-events';
import BaseController from '../../base-controller';

export default class MobileInputController extends BaseController {
  private left: Entity;
  private right: Entity;
  private jump: Entity;
  private leftButton: Phaser.GameObjects.Sprite;
  private rightButton: Phaser.GameObjects.Sprite;
  private jumpButton: Phaser.GameObjects.Sprite;
  private heroBody: Phaser.Physics.Arcade.Body;
  private aimCenterPosition: any;
  private aimIndicator: Phaser.GameObjects.Graphics;

  preload(): void {
    this.F.entityManager.createCollection('input');
  }

  create(): void {
    this.scene.registry.set(REGISTRY.AIM_ANGLE, 0);
    this.createMovementButtons();
  }

  update(): void {
    if (this.heroBody === undefined) {
      this.tryInitializeHero();
    }
    else {
      this.tryInitializeButtons();
    }
    this.updateAimAngle();
  }

  private createMovementButtons(): void {
    // Left button
    this.left = this.F.entityManager.createEntity('input');
    this.F.entityManager.addComponentToEntity(this.left, Position, {
      x: 140 * this.F.config.size.SCALE,
      y: this.F.config.size.SCALED_GAME_HEIGHT - 140 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(this.left, Sprite, {
      key: 'input-left',
      alpha: 0.5,
      scale: 2 * this.F.config.size.SCALE
    });

    // Right button
    this.right = this.F.entityManager.createEntity('input');
    this.F.entityManager.addComponentToEntity(this.right, Position, {
      x: 310 * this.F.config.size.SCALE,
      y: this.F.config.size.SCALED_GAME_HEIGHT - 140 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(this.right, Sprite, {
      key: 'input-right',
      alpha: 0.5,
      scale: 2 * this.F.config.size.SCALE
    });

    // Jump button
    this.jump = this.F.entityManager.createEntity('input');
    this.F.entityManager.addComponentToEntity(this.jump, Position, {
      x: 225 * this.F.config.size.SCALE,
      y: this.F.config.size.SCALED_GAME_HEIGHT - 240 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(this.jump, Sprite, {
      key: 'input-jump',
      alpha: 0.5,
      scale: 2 * this.F.config.size.SCALE
    });
  }

  private createAim(): void {
    this.aimCenterPosition = {
      x: this.F.config.size.SCALED_GAME_WIDTH - 250 * this.F.config.size.SCALE,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2
    };

    const aimCenter = this.F.entityManager.createEntity('input');
    this.F.entityManager.addComponentToEntity(aimCenter, Position, this.aimCenterPosition);
    this.F.entityManager.addComponentToEntity(aimCenter, Sprite, {
      key: 'input-aim',
      alpha: 0.5,
      scale: 2 * this.F.config.size.SCALE
    });

    this.aimIndicator = this.scene.add.graphics();
  }

  private tryInitializeHero(): void {
    const hero = this.F.entityManager.getEntities({ tags: ['hero'] })[0];
    const sprite = this.F.entityManager.getComponentFromEntity(hero, HeroSprite);
    if (sprite.state.container !== undefined && sprite.state.container.body !== undefined) {
      this.heroBody = sprite.state.container.body;
    }
  }

  private tryInitializeButtons(): void {
    if (this.leftButton !== undefined && this.rightButton !== undefined && this.jumpButton !== undefined) {
      return;
    }

    if (this.leftButton === undefined) {
      const leftSprite = this.F.entityManager.getComponentFromEntity(this.left, Sprite);
      if (leftSprite.state.sprite !== undefined) {
        this.leftButton = leftSprite.state.sprite;
        this.setupButton(this.leftButton, () => {
          if (this.heroBody.velocity.x > 0) {
            this.F.subjectService.dispatch(HERO_EVENTS.MOVE_LEFT);
          }
        });
      }
    }

    if (this.rightButton === undefined) {
      const rightSprite = this.F.entityManager.getComponentFromEntity(this.right, Sprite);
      if (rightSprite.state.sprite !== undefined) {
        this.rightButton = rightSprite.state.sprite;
        this.setupButton(this.rightButton, () => {
          if (this.heroBody.velocity.x < 0) {
            this.F.subjectService.dispatch(HERO_EVENTS.MOVE_RIGHT);
          }
        });
      }
    }

    if (this.jumpButton === undefined) {
      const jumpSprite = this.F.entityManager.getComponentFromEntity(this.jump, Sprite);
      if (jumpSprite.state.sprite !== undefined) {
        this.jumpButton = jumpSprite.state.sprite;
        this.setupButton(this.jumpButton, () => {
          this.F.subjectService.dispatch(HERO_EVENTS.JUMP);
        });
      }
    }
  }

  private setupButton(button: Phaser.GameObjects.Sprite, callback: any): void {
    button.setInteractive();
    button.on('pointerdown', () => {
      button.tint = 0xaaaaaa;
      callback();
    });
    button.on('pointerup', () => {
      button.tint = 0xffffff;
    });
  }

  private updateAimAngle(): void {
    if (this.aimIndicator === undefined) {
      this.tryInitializeAimIndicator();
      return;
    }

    const rightThumbPointer = this.scene.input.pointer1.x > this.scene.input.pointer2.x ?
      this.scene.input.pointer1 : this.scene.input.pointer2;
    const rotation: number = Phaser.Math.Angle.Between(
      rightThumbPointer.x,
      rightThumbPointer.y,
      this.aimCenterPosition.x,
      this.aimCenterPosition.y
    );
    const angle: number = Phaser.Math.RadToDeg(rotation) + 180;
    this.scene.registry.set(REGISTRY.AIM_ANGLE, angle);

    this.aimIndicator.clear();
    this.aimIndicator.fillStyle(0x666666, 0.6);
    this.aimIndicator.fillCircle(
      this.aimCenterPosition.x + 70 * this.F.config.size.SCALE * Math.cos(Phaser.Math.DegToRad(angle)),
      this.aimCenterPosition.y + 70 * this.F.config.size.SCALE * Math.sin(Phaser.Math.DegToRad(angle)),
      40 * this.F.config.size.SCALE
    );
  }

  private tryInitializeAimIndicator(): void {
    const background = this.F.entityManager.getEntities({ collectionNames: ['background'] })[0];
    const sprite = this.F.entityManager.getComponentFromEntity(background, Sprite);
    if (sprite.state.sprite !== undefined) {
      this.createAim();
    }
  }
}
