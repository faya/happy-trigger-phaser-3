'use strict';

import HeroSprite from '../../../game-objects/components/hero-sprite';
import REGISTRY from '../../../registry';
import GAME_EVENTS from '../../../services/subject/events/game-events';
import HERO_EVENTS from '../../../services/subject/events/hero-events';
import BaseController from '../../base-controller';

export default class DesktopInputController extends BaseController {
  private leftKey: Phaser.Input.Keyboard.Key;
  private rightKey: Phaser.Input.Keyboard.Key;
  private jumpKey: Phaser.Input.Keyboard.Key;
  private pauseKey: Phaser.Input.Keyboard.Key;
  private heroBody: Phaser.Physics.Arcade.Body;

  create(): void {
    this.scene.registry.set(REGISTRY.AIM_ANGLE, 0);

    this.leftKey = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
    this.rightKey = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
    this.jumpKey = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
    this.pauseKey = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ESC);
  }

  update(): void {
    if (this.heroBody === undefined) {
      this.tryInitialize();
    }

    if (Phaser.Input.Keyboard.JustDown(this.leftKey)) {
      this.F.subjectService.dispatch(HERO_EVENTS.MOVE_LEFT);
    }

    if (Phaser.Input.Keyboard.JustDown(this.rightKey)) {
      this.F.subjectService.dispatch(HERO_EVENTS.MOVE_RIGHT);
    }

    if (Phaser.Input.Keyboard.JustDown(this.jumpKey)) {
      this.F.subjectService.dispatch(HERO_EVENTS.JUMP);
    }

    if (Phaser.Input.Keyboard.JustDown(this.pauseKey)) {
      this.F.subjectService.dispatch(GAME_EVENTS.PAUSED);
      this.pauseKey.isDown = false;
    }

    this.updateAimAngle();
  }

  private tryInitialize(): void {
    const hero = this.F.entityManager.getEntities({ tags: ['hero'] })[0];
    const sprite = this.F.entityManager.getComponentFromEntity(hero, HeroSprite);
    if (sprite.state.container !== undefined && sprite.state.container.body !== undefined) {
      this.heroBody = sprite.state.container.body;
    }
  }

  private updateAimAngle(): void {
    const rotation: number = Phaser.Math.Angle.Between(
      this.scene.input.activePointer.x,
      this.scene.input.activePointer.y,
      this.heroBody.x,
      this.heroBody.y
    );
    this.scene.registry.set(REGISTRY.AIM_ANGLE, Phaser.Math.RadToDeg(rotation) + 180);
  }
}
