'use strict';

import BitmapText from '../../../game-objects/components/bitmap-text';
import EventOnClick from '../../../game-objects/components/event-on-click';
import Health from '../../../game-objects/components/health';
import Position from '../../../game-objects/components/position';
import RegularText from '../../../game-objects/components/regular-text';
import Sprite from '../../../game-objects/components/sprite';
import SpriteAnimation from '../../../game-objects/components/sprite-animation';
import TweenQueue from '../../../game-objects/components/tween';
import WAVE_DATA from '../../../game-objects/waves/waves-data';
import REGISTRY from '../../../registry';
import Component from '../../../services/ecs/components/component';
import Entity from '../../../services/ecs/entities/entity';
import GAME_EVENTS from '../../../services/subject/events/game-events';
import HERO_EVENTS from '../../../services/subject/events/hero-events';
import WAVE_EVENTS from '../../../services/subject/events/wave-events';
import { EVENT_PRIORITIES } from '../../../services/subject/subject-service';
import BaseController from '../../base-controller';

const STYLE_SCORE_CUES = {
  font: '10px Arial',
  fill: '#000000',
  align: 'center'
};

export default class HudController extends BaseController {
  private lifebarSpriteComponents: Component<Sprite>[] = [];
  private scoreTextComponent: Component<BitmapText>;
  private heroHealthComponent: Component<Health>;
  private pauseButton: Entity;
  private shouldCreatePauseButton: boolean = true;

  preload(): void {
    this.F.entityManager.createCollection('hud');
  }

  create(): void {
    this.getHealthReference();
    this.createLifeBar();
    this.createTexts();

    this.F.subjectService.subscribe(WAVE_EVENTS.WAVE_START, this.showWaveTitle.bind(this));
    this.F.subjectService.subscribe(WAVE_EVENTS.ENNEMY_DIED, this.increaseScore.bind(this));
    this.F.subjectService.subscribe(HERO_EVENTS.DIED, this.emptyLifeBar.bind(this));
    this.F.subjectService.subscribe(GAME_EVENTS.PAUSED, this.removePauseButton.bind(this), EVENT_PRIORITIES.HIGH);
    this.F.subjectService.subscribe(GAME_EVENTS.RESUMED, () => this.shouldCreatePauseButton = true);
  }

  update(): void {
    if (this.shouldCreatePauseButton) {
      this.shouldCreatePauseButton = false;
      this.createPauseButton();
    }
    this.updateLifeBar();
    this.updateTexts();
  }

  private getHealthReference(): void {
    const heroEntity = this.F.entityManager.getEntities({ tags: ['hero'] })[0];
    this.heroHealthComponent = this.F.entityManager.getComponentFromEntity(heroEntity, Health);
  }

  private createLifeBar(): void {
    for (let i = 0; i < this.F.config.game.HERO_MAX_HEALTH; i++) {
      const heart: Entity = this.F.entityManager.createEntity('hud');
      this.F.entityManager.addComponentToEntity(heart, Position, {
        x: 20 + i * 50 * this.F.config.size.SCALE,
        y: 20
      });
      const spriteComponent = this.F.entityManager.addComponentToEntity(heart, Sprite, {
        key: 'heart',
        origin: [0, 0],
        hidden: true,
        animations: [
          ['shine', { start: 0, end: 7 }, 10, false]
        ]
      });
      this.F.entityManager.addComponentToEntity(heart, SpriteAnimation, {
        componentType: Sprite,
        stateSpriteKey: 'sprite',
        current: `${heart.id}|shine`,
        repeat: true,
        repeatDelay: 3000
      });

      this.lifebarSpriteComponents.push(spriteComponent);
    }
  }

  private createTexts(): void {
    const score = this.F.entityManager.createEntity('hud');
    this.scoreTextComponent = this.F.entityManager.addComponentToEntity(score, BitmapText, {
      font: 'gameFont',
      text: `${this.scene.registry.get(REGISTRY.SCORE)}`,
      origin: [1, 0],
      size: this.F.config.size.BASE_FONT_SIZE
    });
    this.F.entityManager.addComponentToEntity(score, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH - 60,
      y: 20
    });
  }

  private createPauseButton(): void {
    this.pauseButton = this.F.entityManager.createEntity('hud');
    this.F.entityManager.addComponentToEntity(this.pauseButton, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH - 20,
      y: 20
    });
    this.F.entityManager.addComponentToEntity(this.pauseButton, BitmapText, {
      text: 'II',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE,
      origin: [1, 0]
    });
    this.F.entityManager.addComponentToEntity(this.pauseButton, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 200,
      ease: 'Linear',
      permanent: true,
      loop: true,
      yoyo: true,
      properties: { angle: -1 }
    }] });
    this.F.entityManager.addComponentToEntity(this.pauseButton, EventOnClick, {
      componentType: BitmapText,
      stateObjectKey: 'text',
      event: GAME_EVENTS.PAUSED
    });
  }

  private removePauseButton(): void {
    // cleanup previous button
    const bitmapText = this.F.entityManager.getComponentFromEntity(this.pauseButton, BitmapText);
    bitmapText.state.text.destroy();
    this.F.entityManager.destroyEntities({ ids: [this.pauseButton.id] });
  }

  private updateLifeBar(): void {
    for (let i = 0; i < this.F.config.game.HERO_MAX_HEALTH; i++) {
      this.lifebarSpriteComponents[i].data.hidden = i >= this.heroHealthComponent.state.current;
    }
  }

  private updateTexts(): void {
    this.scoreTextComponent.data.text = `${this.scene.registry.get(REGISTRY.SCORE)}`;
  }

  private increaseScore(ennemy: Entity): void {
    this.scene.registry.set(REGISTRY.SCORE,
      this.scene.registry.get(REGISTRY.SCORE) + this.F.config.game.ENNEMY_KILLED_BONUS);
    // display score cue
    const cue = this.F.entityManager.createEntity('hud');
    const body = this.F.entityManager.getComponentFromEntity(ennemy, Sprite).state.sprite.body;
    const pos = this.F.entityManager
      .addComponentToEntity(cue, Position, { x: body.center.x, y: body.center.y - body.halfHeight });
    const text = this.F.entityManager.addComponentToEntity(cue, RegularText, {
      text: `${this.F.config.game.ENNEMY_KILLED_BONUS}`,
      style: STYLE_SCORE_CUES
    });
    this.F.entityManager.addComponentToEntity(cue, TweenQueue, { items: [{
      componentType: RegularText,
      stateTweenTargetKey: 'text',
      duration: 2000,
      ease: 'Cubic.easeOut',
      properties: { y: pos.data.y - 50, alpha: 0 },
      permanent: false
    }] });
    setTimeout(() => {
      // delete score cue after 2 sec
      text.state.text.destroy();
      this.F.entityManager.destroyEntities({ ids: [cue.id] });
    }, 2000);
  }

  private emptyLifeBar(): void {
    for (const component of this.lifebarSpriteComponents) {
      component.state.sprite.destroy();
    }
  }

  private showWaveTitle(waveIndex: number): void {
    const text: string = waveIndex + 1 === WAVE_DATA.length ? 'FINAL WAVE' : `WAVE ${waveIndex + 1}`;
    const waveTitle = this.F.entityManager.createEntity('hud');
    this.F.entityManager.addComponentToEntity(waveTitle, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH / 2,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 - 200 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(waveTitle, BitmapText, {
      text,
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE * 2
    });
    this.F.entityManager.addComponentToEntity(waveTitle, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 3000,
      ease: 'Back.easeIn',
      permanent: false,
      properties: { y: -500 }
    }] });
  }
}
