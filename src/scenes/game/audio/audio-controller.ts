'use strict';

import { BackgroundAudio, OnEventMultipleAudio } from '../../../game-objects/components/audio';
import GAME_EVENTS from '../../../services/subject/events/game-events';
import HERO_EVENTS from '../../../services/subject/events/hero-events';
import WAVE_EVENTS from '../../../services/subject/events/wave-events';
import BaseController from '../../base-controller';
import SOUND_EFFECT_KEYS, { SOUND_EFFECTS_ATLAS } from '../../common/sound-effects-atlas';

export default class AudioController extends BaseController {
  preload(): void {
    this.F.entityManager.createCollection('audio');
  }

  create(): void {
    const musicIndex = Math.floor(Math.random() * 2) + 1;
    const audio = this.F.entityManager.createEntity('audio');
    this.F.entityManager.addComponentToEntity(audio, BackgroundAudio, {
      key: `music-combat-${musicIndex}`,
      volume: this.F.config.options.MUSIC_VOLUME * 0.5
    });
    this.F.entityManager.addComponentToEntity(audio, OnEventMultipleAudio, { items: [
      {
        key: SOUND_EFFECT_KEYS.MENU,
        volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.MENU].volume,
        events: [GAME_EVENTS.PAUSED]
      },
      {
        key: SOUND_EFFECT_KEYS.JUMP,
        volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.JUMP].volume,
        events: [HERO_EVENTS.JUMP]
      },
      {
        key: SOUND_EFFECT_KEYS.DEATH,
        volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.DEATH].volume,
        events: [WAVE_EVENTS.ENNEMY_DIED]
      },
      {
        key: SOUND_EFFECT_KEYS.EXPLOSION,
        volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.EXPLOSION].volume,
        events: [GAME_EVENTS.EXPLOSION]
      },
      {
        key: SOUND_EFFECT_KEYS.GAME_OVER,
        volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.GAME_OVER].volume,
        events: [HERO_EVENTS.DIED]
      },
      {
        key: SOUND_EFFECT_KEYS.HEALTH,
        volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.HEALTH].volume,
        events: [HERO_EVENTS.BONUS_HEALTH]
      },
      {
        key: SOUND_EFFECT_KEYS.COIN,
        volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.COIN].volume,
        events: [HERO_EVENTS.BONUS_SCORE]
      },
      {
        key: SOUND_EFFECT_KEYS.SHOOT,
        volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.SHOOT].volume,
        events: [HERO_EVENTS.SHOOT]
      }
    ] });
  }
}
