'use strict';

import Sprite from '../../../game-objects/components/sprite';
import State from '../../../game-objects/components/state';
import Component from '../../../services/ecs/components/component';
import Entity from '../../../services/ecs/entities/entity';
import GAME_EVENTS from '../../../services/subject/events/game-events';
import WAVE_EVENTS from '../../../services/subject/events/wave-events';
import { EVENT_PRIORITIES } from '../../../services/subject/subject-service';
import BaseController from '../../base-controller';

export default class EnnemyController extends BaseController {
  private ennemies: Entity[] = [];

  preload(): void {
    this.F.entityManager.createCollection('ennemy');
  }

  create(): void {
    this.F.subjectService.subscribe(WAVE_EVENTS.ENNEMY_DIED, this.killEnnemy.bind(this), EVENT_PRIORITIES.LOW);
  }

  private killEnnemy(ennemy: Entity): void {
    this.F.entityManager.destroyEntities({ ids: [ennemy.id] });
  }
}
