import { ATTACK_BEHAVIORS, ATTACK_BEHAVIORS_KEYS } from '../../../game-objects/behaviors/attack';
import BaseBehavior from '../../../game-objects/behaviors/behavior';
import { MOVEMENT_BEHAVIORS, MOVEMENT_BEHAVIORS_KEYS } from '../../../game-objects/behaviors/movement';
import { BehaviorData } from '../../../game-objects/enemies/ennemy-data';
import Entity from '../../../services/ecs/entities/entity';
import Facade from '../../../services/facade/facade';

'use strict';

export default class BehaviorFactory {
  constructor(private scene: Phaser.Scene, private F: Facade) {}

  build(
    type: string,
    config: BehaviorData,
    externalOptions: any,
    entity: Entity,
    hero: Entity
  ): BaseBehavior {
    const index: { [key: string]: string; } = type === 'movement' ? MOVEMENT_BEHAVIORS_KEYS : ATTACK_BEHAVIORS_KEYS;
    const classes: { [key: string]: {new(s: Phaser.Scene, f: Facade, e: Entity, h: Entity, o: any): BaseBehavior} } =
      type === 'movement' ? MOVEMENT_BEHAVIORS : ATTACK_BEHAVIORS;
    const options = Object.assign(
      JSON.parse(JSON.stringify(config.options)),
      JSON.parse(JSON.stringify(externalOptions))
    );
    return (new classes[index[config.key]](
      this.scene,
      this.F,
      entity,
      hero,
      options
    ) as BaseBehavior);
  }
}
