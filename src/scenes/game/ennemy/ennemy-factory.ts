'use strict';

import ArcadeSprite from '../../../game-objects/components/arcade-sprite';
import BehaviorCollection from '../../../game-objects/components/behavior-collection';
import Collidable from '../../../game-objects/components/collidable';
import ControllableByMatrix from '../../../game-objects/components/controllable-by-matrix';
import DamageOverlaping from '../../../game-objects/components/damage-overlaping';
import Health from '../../../game-objects/components/health';
import Position from '../../../game-objects/components/position';
import Sprite from '../../../game-objects/components/sprite';
import SpriteAnimation from '../../../game-objects/components/sprite-animation';
import State from '../../../game-objects/components/state';
import StateAnimationControl from '../../../game-objects/components/state-animation-control';
import { BehaviorData, EnnemyData } from '../../../game-objects/enemies/ennemy-data';
import ENTITY_STATE from '../../../game-objects/misc/entity-state';
import Entity from '../../../services/ecs/entities/entity';
import Facade from '../../../services/facade/facade';
import GAME_EVENTS from '../../../services/subject/events/game-events';
import WAVE_EVENTS from '../../../services/subject/events/wave-events';

export default class EnnemyFactory {
  constructor(private F: Facade) {}

  build(config: EnnemyData, xPos: number, yPos: number): Entity {
    const ennemy: Entity = this.F.entityManager.createEntity('ennemy');

    this.F.entityManager.addComponentToEntity(ennemy, Health, {
      max: config.health,
      eventOnDeath: WAVE_EVENTS.ENNEMY_DIED
    });
    const x = xPos * this.F.config.size.SCALED_TILE_SIZE;
    const y = yPos * this.F.config.size.SCALED_TILE_SIZE;
    this.F.entityManager.addComponentToEntity(ennemy, Position, { x, y });
    this.F.entityManager.addComponentToEntity(ennemy, Sprite, {
      key: config.assetKey,
      animations: [
        [ENTITY_STATE.IDLE_LEFT, { frames: [1] }, 10, false],
        [ENTITY_STATE.RUNNING_LEFT, { frames: [0, 1] }, 10, true],
        [ENTITY_STATE.RUNNING_RIGHT, { frames: [2, 3] }, 10, true],
        [ENTITY_STATE.JUMPING_LEFT, { frames: [0] }, 10, false],
        [ENTITY_STATE.JUMPING_RIGHT, { frames: [2] }, 10, false],
        [ENTITY_STATE.HURT_LEFT, { frames: [4, 5, 4, 5] }, 10, false],
        [ENTITY_STATE.HURT_RIGHT, { frames: [6, 7, 6, 7] }, 10, false]
      ]
    });
    this.F.entityManager.addComponentToEntity(ennemy, ArcadeSprite, { collidesWorldBounds: true });
    this.F.entityManager.addComponentToEntity(ennemy, SpriteAnimation,
      { componentType: Sprite, stateSpriteKey: 'sprite' });
    this.F.entityManager.addComponentToEntity(ennemy, State, {
      componentType: Sprite,
      stateComponentKey: 'sprite',
      jumpCharges: 1
    });
    this.F.entityManager.addComponentToEntity(ennemy, StateAnimationControl, {
      animatedStates: [ENTITY_STATE.IDLE_LEFT, ENTITY_STATE.RUNNING_LEFT, ENTITY_STATE.RUNNING_RIGHT,
        ENTITY_STATE.JUMPING_LEFT, ENTITY_STATE.JUMPING_RIGHT, ENTITY_STATE.HURT_LEFT, ENTITY_STATE.JUMPING_RIGHT]
    });
    this.F.entityManager.addComponentToEntity(ennemy, ControllableByMatrix, {
      componentType: Sprite,
      stateBodyKey: 'sprite',
      horizontalVelocity: config.movementBehavior.options.velocity,
      jumpVelocity: config.movementBehavior.options.jumpVelocity
    });
    this.F.entityManager.addComponentToEntity(ennemy, Collidable, {
      componentType: Sprite,
      stateCollidableKey: 'sprite',
      collidesWith: [{
        filters: { tags: ['platform'] },
        componentType: Sprite,
        stateCollidableKey: 'sprite',
        collisionEvent: GAME_EVENTS.ENTITY_TOUCHED_PLATFORM
      }]
    });
    this.F.entityManager.addComponentToEntity(ennemy, DamageOverlaping, {
      targetFilters: { tags: ['hero'] },
      damageValue: this.F.config.game.ENNEMY_COLLISION_DAMAGE,
      destroyOnOverlap: false
    });

    return ennemy;
  }

  buildBehaviors(ennemy: Entity, config: EnnemyData, moveOptions: any, attackOptions: any): void {
    this.F.entityManager.addComponentToEntity(ennemy, BehaviorCollection,
      { items: this.getBehaviorCollection(config, moveOptions, attackOptions) });
  }

  private getBehaviorCollection(
    config: EnnemyData,
    moveOptions: any,
    attackOptions: any
  ): [string, BehaviorData, any][] {
    const output: [string, BehaviorData, any][] = [
      ['movement', config.movementBehavior, moveOptions],
      ['attack', config.attackBehavior, attackOptions]
    ];
    return output;
  }
}
