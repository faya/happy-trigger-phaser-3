'use strict';

import Facade from '../../services/facade/facade';
import SCENE_EVENTS from '../../services/subject/events/scene-events';
import BaseScene from '../base-scene';

export default class BootScene extends BaseScene {
  constructor(facade: Facade) {
    super({
      key: facade.config.scenes.BOOT
    }, facade);
  }

  create(): void {
    super.create();
    this.F.subjectService.dispatch(SCENE_EVENTS.START_LOADING, this.scene.key);
  }
}
