'use strict';

import { BackgroundAudio, OnEventAudio } from '../../game-objects/components/audio';
import BitmapText from '../../game-objects/components/bitmap-text';
import EventOnClick from '../../game-objects/components/event-on-click';
import Position from '../../game-objects/components/position';
import TweenQueue from '../../game-objects/components/tween';
import AudioSystem from '../../game-objects/systems/audio-system';
import BitmapTextSystem from '../../game-objects/systems/bitmap-text-system';
import EventOnClickSystem from '../../game-objects/systems/event-on-click-system';
import SpriteSystem from '../../game-objects/systems/sprite-system';
import TweenSystem from '../../game-objects/systems/tween-system';
import Component from '../../services/ecs/components/component';
import Facade from '../../services/facade/facade';
import SCENE_EVENTS from '../../services/subject/events/scene-events';
import BaseScene from '../base-scene';
import BackgroundFactory from '../common/background-factory';
import SOUND_EFFECT_KEYS, { SOUND_EFFECTS_ATLAS } from '../common/sound-effects-atlas';

export default class GameOverScene extends BaseScene {
  private gameScoreTweens: Component<TweenQueue>;
  private highScoreTweens: Component<TweenQueue>;
  private score: number = 0;

  constructor(facade: Facade) {
    super({
      key: facade.config.scenes.GAME_OVER
    }, facade);
  }

  init(data?: any): void {
    super.init();
    this.registerSystem(new SpriteSystem(this, this.F));
    this.registerSystem(new BitmapTextSystem(this, this.F));
    this.registerSystem(new TweenSystem(this, this.F));
    this.registerSystem(new EventOnClickSystem(this, this.F));
    this.registerSystem(new AudioSystem(this, this.F));
    this.score = data.score;
  }

  preload(): void {
    super.preload();
    this.F.entityManager.createCollection('gameover');
    this.F.subjectService.subscribe('gameover:back', this.changeScene.bind(this, SCENE_EVENTS.START_TITLE));
    this.F.subjectService.subscribe('gameover:retry', this.changeScene.bind(this, SCENE_EVENTS.START_GAME));
  }

  create(): void {
    new BackgroundFactory(this.F).build('gameover');
    this.createTexts();
    this.createTextButtons();
    this.playMusic();
    super.create();
  }

  private changeScene(event: string): void {
    this.gameScoreTweens.data.items.push({
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 1500,
      ease: 'Back.easeIn',
      permanent: false,
      properties: { x: this.F.config.size.SCALED_GAME_WIDTH + 600 }
    });
    this.highScoreTweens.data.items.push({
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 1500,
      ease: 'Back.easeIn',
      permanent: false,
      properties: { x: this.F.config.size.SCALED_GAME_WIDTH + 600 }
    });
    this.F.subjectService.delayedDispatch(event, 2000, this.scene.key);
  }

  private createTexts(): void {
    const gameOver = this.F.entityManager.createEntity('gameover');
    this.F.entityManager.addComponentToEntity(gameOver, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH / 2,
      y: 200 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(gameOver, BitmapText, {
      text: 'GAME OVER',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE * 2.5,
      origin: [0.5, 0]
    });

    const gameScore = this.F.entityManager.createEntity('gameover');
    this.F.entityManager.addComponentToEntity(gameScore, Position, {
      x: -600,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 - 50 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(gameScore, BitmapText, {
      text: `score : ${this.score}`,
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE * 1.5
    });

    const highScore = this.F.entityManager.createEntity('gameover');
    this.F.entityManager.addComponentToEntity(highScore, Position, {
      x: -600,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 + 50 * this.F.config.size.SCALE
    });
    const highscoreValue = localStorage['highscore'] || 0; // tslint:disable-line
    this.F.entityManager.addComponentToEntity(highScore, BitmapText, {
      text: `best score : ${highscoreValue}`,
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE * 1.5
    });

    this.gameScoreTweens = this.F.entityManager.addComponentToEntity(gameScore, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 1500,
      ease: 'Back.easeOut',
      permanent: false,
      properties: { x: this.F.config.size.SCALED_GAME_WIDTH / 2 }
    }] });
    this.highScoreTweens = this.F.entityManager.addComponentToEntity(highScore, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 1500,
      ease: 'Back.easeOut',
      permanent: false,
      properties: { x: this.F.config.size.SCALED_GAME_WIDTH / 2 }
    }] });
  }

  private createTextButtons(): void {
    const backButton = this.F.entityManager.createEntity('gameover');
    this.F.entityManager.addComponentToEntity(backButton, Position, {
      x: 100 * this.F.config.size.SCALE,
      y: this.F.config.size.SCALED_GAME_HEIGHT - 100 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(backButton, BitmapText, {
      text: 'back',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE * 2,
      origin: [0, 1]
    });
    this.F.entityManager.addComponentToEntity(backButton, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 250,
      ease: 'Linear',
      permanent: true,
      properties: { angle: -1 },
      delay: 1500,
      yoyo: true,
      loop: true
    }] });
    this.F.entityManager.addComponentToEntity(backButton, EventOnClick, {
      componentType: BitmapText,
      stateObjectKey: 'text',
      event: 'gameover:back'
    });

    const retryButton = this.F.entityManager.createEntity('gameover');
    this.F.entityManager.addComponentToEntity(retryButton, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH - 100 * this.F.config.size.SCALE,
      y: this.F.config.size.SCALED_GAME_HEIGHT - 100 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(retryButton, BitmapText, {
      text: 'retry',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE * 2,
      origin: [1, 1]
    });
    this.F.entityManager.addComponentToEntity(retryButton, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 250,
      ease: 'Linear',
      permanent: true,
      properties: { angle: 1 },
      delay: 1500,
      yoyo: true,
      loop: true
    }] });
    this.F.entityManager.addComponentToEntity(retryButton, EventOnClick, {
      componentType: BitmapText,
      stateObjectKey: 'text',
      event: 'gameover:retry'
    });
  }

  private playMusic(): void {
    const audio = this.F.entityManager.createEntity('gameover');
    this.F.entityManager.addComponentToEntity(audio, BackgroundAudio,
      { key: 'music-menu', volume: this.F.config.options.MUSIC_VOLUME });
    this.F.entityManager.addComponentToEntity(audio, OnEventAudio, {
      key: SOUND_EFFECT_KEYS.MENU,
      volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.MENU].volume,
      events: ['gameover:retry', 'gameover:back']
    });
  }
}
