'use strict';

import { BackgroundAudio, OnEventAudio } from '../../game-objects/components/audio';
import BitmapText from '../../game-objects/components/bitmap-text';
import EventOnClick from '../../game-objects/components/event-on-click';
import Position from '../../game-objects/components/position';
import TweenQueue from '../../game-objects/components/tween';
import AudioSystem from '../../game-objects/systems/audio-system';
import BitmapTextSystem from '../../game-objects/systems/bitmap-text-system';
import EventOnClickSystem from '../../game-objects/systems/event-on-click-system';
import RegularTextSystem from '../../game-objects/systems/regular-text-system';
import SpriteSystem from '../../game-objects/systems/sprite-system';
import TweenSystem from '../../game-objects/systems/tween-system';
import Facade from '../../services/facade/facade';
import SCENE_EVENTS from '../../services/subject/events/scene-events';
import BaseScene from '../base-scene';
import BackgroundFactory from '../common/background-factory';
import SOUND_EFFECT_KEYS, { SOUND_EFFECTS_ATLAS } from '../common/sound-effects-atlas';

export default class CreditsScene extends BaseScene {
  private score: number = 0;

  constructor(facade: Facade) {
    super({
      key: facade.config.scenes.CREDITS
    }, facade);
  }

  init(data?: any): void {
    super.init();
    this.registerSystem(new SpriteSystem(this, this.F));
    this.registerSystem(new BitmapTextSystem(this, this.F));
    this.registerSystem(new RegularTextSystem(this, this.F));
    this.registerSystem(new TweenSystem(this, this.F));
    this.registerSystem(new EventOnClickSystem(this, this.F));
    this.registerSystem(new AudioSystem(this, this.F));
    this.score = data.score;
  }

  preload(): void {
    super.preload();
    this.F.entityManager.createCollection('credits');
    this.F.subjectService.subscribe('credits:back',
      () => this.F.subjectService.dispatch(SCENE_EVENTS.START_TITLE, this.scene.key));
    this.F.subjectService.subscribe('credits:restart',
      () => this.F.subjectService.dispatch(SCENE_EVENTS.START_GAME, this.scene.key));
  }

  create(): void {
    new BackgroundFactory(this.F).build('credits');
    this.createTexts();
    this.createTextButtons();
    this.playMusic();
    super.create();
  }

  private createTexts(): void {
    // Congratulations
    const congrats = this.F.entityManager.createEntity('credits');
    this.F.entityManager.addComponentToEntity(congrats, Position, {
      x: -600,
      y: 150 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(congrats, BitmapText, {
      text: 'CONGRATULATIONS !',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE * 1.5
    });
    this.F.entityManager.addComponentToEntity(congrats, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 1500,
      ease: 'Back.easeOut',
      permanent: false,
      properties: { x: this.F.config.size.SCALED_GAME_WIDTH / 2 }
    }] });
    // You won
    const youWon = this.F.entityManager.createEntity('credits');
    this.F.entityManager.addComponentToEntity(youWon, Position, {
      x: -600,
      y: 220 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(youWon, BitmapText, {
      text: 'you beat the game',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE
    });
    this.F.entityManager.addComponentToEntity(youWon, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 1500,
      ease: 'Back.easeOut',
      permanent: false,
      properties: { x: this.F.config.size.SCALED_GAME_WIDTH / 2 }
    }] });

    // A game by Nico F
    const credits = this.F.entityManager.createEntity('credits');
    this.F.entityManager.addComponentToEntity(credits, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH + 600,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 + 150 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(credits, BitmapText, {
      text: 'a game by nico f',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE
    });
    this.F.entityManager.addComponentToEntity(credits, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 1500,
      ease: 'Back.easeOut',
      permanent: false,
      properties: { x: this.F.config.size.SCALED_GAME_WIDTH / 2 }
    }] });
    // personal website
    const website = this.F.entityManager.createEntity('credits');
    this.F.entityManager.addComponentToEntity(website, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH + 600,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 + 190 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(website, BitmapText, {
      text: 'nfernand.github.io',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE * 0.7
    });
    this.F.entityManager.addComponentToEntity(website, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 1500,
      ease: 'Back.easeOut',
      permanent: false,
      properties: { x: this.F.config.size.SCALED_GAME_WIDTH / 2 }
    }] });
    // assets
    const art = this.F.entityManager.createEntity('credits');
    this.F.entityManager.addComponentToEntity(art, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH + 600,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 + 230 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(art, BitmapText, {
      text: 'art by bevouliin (bevouliin.com)',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE * 0.7
    });
    this.F.entityManager.addComponentToEntity(art, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 1500,
      ease: 'Back.easeOut',
      permanent: false,
      properties: { x: this.F.config.size.SCALED_GAME_WIDTH / 2 }
    }] });

    // Score
    const score = this.F.entityManager.createEntity('credits');
    this.F.entityManager.addComponentToEntity(score, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH / 2,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 - 10
    });
    this.F.entityManager.addComponentToEntity(score, BitmapText, {
      text: `score : ${this.score}`,
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE * 2
    });
    this.F.entityManager.addComponentToEntity(score, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 600,
      ease: 'Linear',
      permanent: true,
      loop: true,
      yoyo: true,
      properties: { y: this.F.config.size.SCALED_GAME_HEIGHT / 2 + 10 }
    }] });
  }

  private createTextButtons(): void {
    // Back button
    const backButton = this.F.entityManager.createEntity('credits');
    this.F.entityManager.addComponentToEntity(backButton, Position, {
      x: 100 * this.F.config.size.SCALE,
      y: this.F.config.size.SCALED_GAME_HEIGHT - 100 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(backButton, BitmapText, {
      text: 'back',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE,
      origin: [0, 1]
    });
    this.F.entityManager.addComponentToEntity(backButton, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 250,
      ease: 'Linear',
      permanent: true,
      properties: { angle: -1 },
      delay: 1500,
      yoyo: true,
      loop: true
    }] });
    this.F.entityManager.addComponentToEntity(backButton, EventOnClick, {
      componentType: BitmapText,
      stateObjectKey: 'text',
      event: 'credits:back'
    });

    // Restart button
    const restartButton = this.F.entityManager.createEntity('credits');
    this.F.entityManager.addComponentToEntity(restartButton, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH - 100 * this.F.config.size.SCALE,
      y: this.F.config.size.SCALED_GAME_HEIGHT - 100 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(restartButton, BitmapText, {
      text: 'retry',
      font: 'gameFont',
      size: this.F.config.size.BASE_FONT_SIZE,
      origin: [1, 1]
    });
    this.F.entityManager.addComponentToEntity(restartButton, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      duration: 250,
      ease: 'Linear',
      permanent: true,
      properties: { angle: 1 },
      delay: 1500,
      yoyo: true,
      loop: true
    }] });
    this.F.entityManager.addComponentToEntity(restartButton, EventOnClick, {
      componentType: BitmapText,
      stateObjectKey: 'text',
      event: 'credits:restart'
    });
  }

  private playMusic(): void {
    const audio = this.F.entityManager.createEntity('credits');
    this.F.entityManager.addComponentToEntity(audio, BackgroundAudio,
      { key: 'music-menu', volume: this.F.config.options.MUSIC_VOLUME });
    this.F.entityManager.addComponentToEntity(audio, OnEventAudio, {
      key: SOUND_EFFECT_KEYS.MENU,
      volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.MENU].volume,
      events: ['credits:restart', 'credits:back']
    });
  }
}
