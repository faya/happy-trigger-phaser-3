import Position from '../../game-objects/components/position';
import Sprite from '../../game-objects/components/sprite';
import Facade from '../../services/facade/facade';

'use strict';

export default class BackgroundFactory {
  constructor(private F: Facade) {}

  build(collectionName: string): void {
    for (let i = 1; i <= 4; i++) {
      const bgEntity = this.F.entityManager.createEntity(collectionName);
      this.F.entityManager.addComponentToEntity(bgEntity, Position, { x: 0, y: 0 });
      this.F.entityManager.addComponentToEntity(bgEntity, Sprite, { key: `background-${i}`, origin: [0, 0] });
    }
  }
}
