'use strict';

const SOUND_EFFECT_KEYS: { [key: string]: string; }  = {
  COIN: 'SOUND:COIN',
  DEATH: 'SOUND:DEATH',
  EXPLOSION: 'SOUND:EXPLOSION',
  GAME_OVER: 'SOUND:GAME_OVER',
  HEALTH: 'SOUND:HEALTH',
  JUMP: 'SOUND:JUMP',
  SHOOT: 'SOUND:SHOOT',
  MENU: 'SOUND:MENU'
};

export default SOUND_EFFECT_KEYS;

export const SOUND_EFFECTS_ATLAS = {
  [SOUND_EFFECT_KEYS.COIN]: {
    path: './assets/music/sounds/coin.wav',
    volume: 0.3
  },
  [SOUND_EFFECT_KEYS.DEATH]: {
    path: './assets/music/sounds/death.wav',
    volume: 0.8
  },
  [SOUND_EFFECT_KEYS.EXPLOSION]: {
    path: './assets/music/sounds/explosion.wav',
    volume: 1.0
  },
  [SOUND_EFFECT_KEYS.GAME_OVER]: {
    path: './assets/music/sounds/game-over.mp3',
    volume: 0.8
  },
  [SOUND_EFFECT_KEYS.HEALTH]: {
    path: './assets/music/sounds/health.wav',
    volume: 0.5
  },
  [SOUND_EFFECT_KEYS.JUMP]: {
    path: './assets/music/sounds/jump.wav',
    volume: 0.3
  },
  [SOUND_EFFECT_KEYS.SHOOT]: {
    path: './assets/music/sounds/shoot.ogg',
    volume: 0.5
  },
  [SOUND_EFFECT_KEYS.MENU]: {
    path: './assets/music/sounds/menu-interaction.wav',
    volume: 0.8
  },
};
