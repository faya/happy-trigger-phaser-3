'use strict';

import ENNEMY_DATA from '../../game-objects/enemies/ennemy-data';
import Facade from '../../services/facade/facade';
import SCENE_EVENTS from '../../services/subject/events/scene-events';
import BaseScene from '../base-scene';
import SOUND_EFFECT_KEYS, { SOUND_EFFECTS_ATLAS } from '../common/sound-effects-atlas';

export default class LoadingScene extends BaseScene {
  private progressBar: Phaser.GameObjects.Graphics;
  private progressBarContainer: Phaser.GameObjects.Graphics;

  constructor(facade: Facade) {
    super({
      key: facade.config.scenes.LOADING
    }, facade);
  }

  preload(): void {
    super.preload();
    this.buildProgressBar();
    this.createTexts();

    // scene
    this.load.image('tile', './assets/scene/tile.png');
    this.load.image('background-1', './assets/scene/background-1.png');
    this.load.image('background-2', './assets/scene/background-2.png');
    this.load.image('background-3', './assets/scene/background-3.png');
    this.load.image('background-4', './assets/scene/background-4.png');
    this.load.image('instructions', './assets/scene/instructions.png');

    // hero
    this.load.image('hero-container', './assets/hero/hero-container.png');
    this.load.spritesheet('hero', './assets/hero/hero-spritesheet.png', { frameWidth: 83, frameHeight: 120 });
    this.load.spritesheet('hero-weapon-arm', './assets/hero/hero-arm-spritesheet.png',
      { frameWidth: 64, frameHeight: 46 });
    this.load.image('crosshair', './assets/hero/crosshair.png');

    // misc
    this.load.spritesheet('projectiles', './assets/projectiles/projectiles-spritesheet.png', { frameWidth: 30 });
    this.load.spritesheet('heart', './assets/misc/heart.png', { frameWidth: 40 });
    this.load.spritesheet('coin', './assets/misc/coin.png', { frameWidth: 40 });
    this.load.image('spawn-cue', './assets/misc/spawn.png');

    // enemies
    for (const key of Object.keys(ENNEMY_DATA)) {
      const data = ENNEMY_DATA[key];
      this.load.spritesheet(
        data.assetKey,
        `./assets/ennemies/${data.assetKey}.png`,
        { frameWidth: data.spriteWidth, frameHeight: data.spriteHeight });
    }

    // fonts
    this.load.bitmapFont('gameFont', './assets/fonts/font.png', './assets/fonts/font.fnt');

    // musics
    this.load.audio('music-menu', './assets/music/menu.ogg');
    this.load.audio('music-combat-1', './assets/music/combat-1.ogg');
    this.load.audio('music-combat-2', './assets/music/combat-2.ogg');
    this.load.audio('music-boss', './assets/music/boss.ogg');

    // sound effects
    for (const key in SOUND_EFFECT_KEYS) {
      this.load.audio(SOUND_EFFECT_KEYS[key], SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS[key]].path);
    }

    // mobile inputs
    if (this.F.config.game.IS_MOBILE) {
      this.load.image('input-left', './assets/input/left.png');
      this.load.image('input-right', './assets/input/right.png');
      this.load.image('input-jump', './assets/input/up.png');
      this.load.image('input-aim', './assets/input/aim.png');
    }
  }

  create(): void {
    super.create();
    this.F.subjectService.dispatch(SCENE_EVENTS.START_TITLE, this.scene.key);
  }

  private buildProgressBar(): void {
    this.progressBar = this.add.graphics();
    this.progressBarContainer = this.add.graphics();
    this.progressBarContainer.fillStyle(0x222222, 0.8);
    this.progressBarContainer.fillRect(
      this.F.config.size.SCALED_GAME_WIDTH / 2 - 180,
      this.F.config.size.SCALED_GAME_HEIGHT / 2 - 20,
      360,
      40
    );
    this.load.on('progress', (value: number) => {
      this.progressBar.clear();
      this.progressBar.fillStyle(0xffffff, 1);
      this.progressBar.fillRect(
        this.F.config.size.SCALED_GAME_WIDTH / 2 - 175,
        this.F.config.size.SCALED_GAME_HEIGHT / 2 - 15,
        350 * value,
        30
      );
    });
  }

  private createTexts(): void {
    const loadingText = this.make.text({
      x: this.F.config.size.SCALED_GAME_WIDTH / 2,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 - 50,
      text: 'Loading...',
      style: {
        font: '20px monospace',
        fill: '#ffffff'
      }
    });
    loadingText.setOrigin(0.5, 0.5);

    const progressText = this.make.text({
      x: this.F.config.size.SCALED_GAME_WIDTH / 2,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 + 50,
      text: '',
      style: {
        font: '18px monospace',
        fill: '#ffffff'
      }
    });
    progressText.setOrigin(0.5, 0.5);

    this.load.on('fileprogress', (file: any) => {
      progressText.setText(`loading asset: ${file.key}`);
    });
  }
}
