'use strict';

import { BackgroundAudio, OnEventAudio } from '../../game-objects/components/audio';
import BitmapText from '../../game-objects/components/bitmap-text';
import Position from '../../game-objects/components/position';
import Sprite from '../../game-objects/components/sprite';
import TweenQueue from '../../game-objects/components/tween';
import ENNEMY_DATA, { ENNEMY_KEYS } from '../../game-objects/enemies/ennemy-data';
import { Direction } from '../../game-objects/misc/direction';
import ArcadeSpriteSystem from '../../game-objects/systems/arcade-sprite-system';
import AudioSystem from '../../game-objects/systems/audio-system';
import BehaviorSystem from '../../game-objects/systems/behavior-system';
import BitmapTextSystem from '../../game-objects/systems/bitmap-text-system';
import ControllableByMatrixSystem from '../../game-objects/systems/controllable-by-matrix-system';
import SpriteAnimationSystem from '../../game-objects/systems/sprite-animation-system';
import SpriteSystem from '../../game-objects/systems/sprite-system';
import StateAnimationControlSystem from '../../game-objects/systems/state-animation-control-system';
import StateSystem from '../../game-objects/systems/state-system';
import TweenSystem from '../../game-objects/systems/tween-system';
import Entity from '../../services/ecs/entities/entity';
import Facade from '../../services/facade/facade';
import SCENE_EVENTS from '../../services/subject/events/scene-events';
import BaseScene from '../base-scene';
import BackgroundFactory from '../common/background-factory';
import SOUND_EFFECT_KEYS, { SOUND_EFFECTS_ATLAS } from '../common/sound-effects-atlas';
import EnnemyFactory from '../game/ennemy/ennemy-factory';

export default class TitleScene extends BaseScene {
  private ennemies: Entity[] = [];

  constructor(facade: Facade) {
    super({
      key: facade.config.scenes.TITLE
    }, facade);
  }

  init(): void {
    super.init();
    this.registerSystem(new SpriteSystem(this, this.F));
    this.registerSystem(new ArcadeSpriteSystem(this, this.F));
    this.registerSystem(new BitmapTextSystem(this, this.F));
    this.registerSystem(new StateSystem(this, this.F));
    this.registerSystem(new StateAnimationControlSystem(this, this.F));
    this.registerSystem(new SpriteAnimationSystem(this, this.F));
    this.registerSystem(new ControllableByMatrixSystem(this, this.F));
    this.registerSystem(new BehaviorSystem(this, this.F));
    this.registerSystem(new TweenSystem(this, this.F));
    this.registerSystem(new AudioSystem(this, this.F));
  }

  preload(): void {
    super.preload();
    this.F.entityManager.createCollection('title');
    this.F.entityManager.createCollection('ennemy');
  }

  create(): void {
    this.ennemies = [];
    new BackgroundFactory(this.F).build('title');
    this.createInstructions();
    this.createTexts();
    this.createEnnemies();
    this.playMusic();
    this.listenEvents();
    // here we run super.create() after creating title entities
    // in order to enable the systems to operate on them
    super.create();
  }

  private createInstructions(): void {
    const instructions = this.F.entityManager.createEntity('title');
    this.F.entityManager.addComponentToEntity(instructions, Position, { x: 0, y: 0 });
    this.F.entityManager.addComponentToEntity(instructions, Sprite, { key: 'instructions', origin: [0, 0] });
  }

  private createTexts(): void {
    const title = this.F.entityManager.createEntity('title');
    this.F.entityManager.addComponentToEntity(title, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH / 2,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 - 100 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(title, BitmapText, {
      font: 'gameFont',
      text: 'Happy\nTrigger',
      size: this.F.config.size.BASE_FONT_SIZE * 3,
      align: Phaser.GameObjects.BitmapText.ALIGN_CENTER
    });
    this.F.entityManager.addComponentToEntity(title, TweenQueue, { items: [{
      componentType: BitmapText,
      stateTweenTargetKey: 'text',
      properties: { y: this.F.config.size.SCALED_GAME_HEIGHT / 2 - 80 * this.F.config.size.SCALE },
      duration: 600,
      ease: 'Linear',
      yoyo: true,
      loop: true,
      permanent: true
    }] });

    const subtitle = this.F.entityManager.createEntity('title');
    this.F.entityManager.addComponentToEntity(subtitle, Position, {
      x: this.F.config.size.SCALED_GAME_WIDTH / 2,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2 + 100 * this.F.config.size.SCALE
    });
    this.F.entityManager.addComponentToEntity(subtitle, BitmapText, {
      font: 'gameFont',
      text: 'click to start',
      size: this.F.config.size.BASE_FONT_SIZE * 2
    });

    const highscoreValue = localStorage['highscore'] || 0; // tslint:disable-line
    const highscore = this.F.entityManager.createEntity('title');
    this.F.entityManager.addComponentToEntity(highscore, Position,
      { x: this.F.config.size.SCALED_GAME_WIDTH - 20, y: 20 });
    this.F.entityManager.addComponentToEntity(highscore, BitmapText, {
      font: 'gameFont',
      text: `Highest score : ${highscoreValue}`,
      size: this.F.config.size.BASE_FONT_SIZE,
      origin: [1, 0]
    });
  }

  private createEnnemies(): void {
    const factory = new EnnemyFactory(this.F);
    this.ennemies.push(factory.build(ENNEMY_DATA[ENNEMY_KEYS.BLOB_PINK], 5, 12));
    this.ennemies.push(factory.build(ENNEMY_DATA[ENNEMY_KEYS.CYCLOP_BLUE], 15, 12));
    this.ennemies.push(factory.build(ENNEMY_DATA[ENNEMY_KEYS.BIRD_BLUE], 10, 3));
    factory.buildBehaviors(this.ennemies[0], ENNEMY_DATA[ENNEMY_KEYS.BLOB_PINK], { direction: Direction.RIGHT }, {});
    factory.buildBehaviors(this.ennemies[1], ENNEMY_DATA[ENNEMY_KEYS.CYCLOP_BLUE], { direction: Direction.LEFT }, {});
    factory.buildBehaviors(this.ennemies[2], ENNEMY_DATA[ENNEMY_KEYS.BIRD_BLUE], { direction: Direction.LEFT }, {});
  }

  private playMusic(): void {
    const audio = this.F.entityManager.createEntity('title');
    this.F.entityManager.addComponentToEntity(audio, BackgroundAudio,
      { key: 'music-menu', volume: this.F.config.options.MUSIC_VOLUME });
    this.F.entityManager.addComponentToEntity(audio, OnEventAudio, {
      key: SOUND_EFFECT_KEYS.MENU,
      volume: SOUND_EFFECTS_ATLAS[SOUND_EFFECT_KEYS.MENU].volume,
      events: [SCENE_EVENTS.START_GAME]
    });
  }

  private listenEvents(): void {
    setTimeout(() => this.input.once('pointerup', () => {
      this.F.subjectService.dispatch(SCENE_EVENTS.START_GAME, this.scene.key);
    }, true), 500);
  }
}
