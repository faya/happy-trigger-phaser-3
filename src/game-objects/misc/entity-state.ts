'use strict';

const ENTITY_STATE = {
  IDLE_RIGHT: 'state:idle-right',
  IDLE_LEFT: 'state:idle-left',
  RUNNING_RIGHT: 'state:running-right',
  RUNNING_LEFT: 'state:running-left',
  JUMPING_RIGHT: 'state:jumping-right',
  JUMPING_LEFT: 'state:jumping-left',
  HURT_RIGHT: 'state:hurt-right',
  HURT_LEFT: 'state:hurt-left'
};

export default ENTITY_STATE;
