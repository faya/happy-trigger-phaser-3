'use strict';

export class Direction  {
  static LEFT: string = 'direction:left';
  static RIGHT: string = 'direction:right';
  static UP: string = 'direction:up';
  static DOWN: string = 'direction:down';

  static getDirectionFactor(direction: string) {
    switch (direction) {
      case Direction.LEFT:
      case Direction.UP:
        return -1;
      case Direction.RIGHT:
      case Direction.DOWN:
        return 1;
      default:
        return 0;
    }
  }

  static getDirectionsFromVelocity(xVel: number, yVel: number): { [key: string]: boolean; } {
    return {
      [Direction.LEFT]: xVel < 0,
      [Direction.RIGHT]: xVel > 0,
      [Direction.UP]: yVel < 0,
      [Direction.DOWN]: yVel > 0
    };
  }

  static getHorizontalDirectionFromAngle(angle: number): string {
    return angle > 90 && angle < 270 ? Direction.LEFT : Direction.RIGHT;
  }

  static getAngle(direction: string): number {
    switch (direction) {
      case Direction.LEFT:
        return 180;
      case Direction.RIGHT:
        return 0;
      case Direction.UP:
        return -90;
      case Direction.DOWN:
        return 90;
      default:
        throw new Error(`Invalid direction ${direction}`);
    }
  }
}
