'use strict';

import PatrolBlindBehavior from './patrol-blind';

/*
  Options:
  - direction: string
  - jumpDelay: number
*/
export default class PatrolJumpBehavior extends PatrolBlindBehavior {
  private lastJumpTick: number;

  startUp(): void {
    super.startUp();
    this.lastJumpTick = this.scene.time.now;
  }

  apply(): void {
    const currentTick = this.scene.time.now;
    if (currentTick - this.lastJumpTick >= this.options.jumpDelay) {
      this.lastJumpTick = currentTick;
      this.entityControlMatrix.data.jump = true;
    }
  }
}
