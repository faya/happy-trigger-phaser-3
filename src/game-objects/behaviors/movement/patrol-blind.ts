'use strict';

import { Direction } from '../../misc/direction';
import BaseBehavior from '../behavior';

/*
  Options:
  - direction: string
*/

export default class PatrolBlindBehavior extends BaseBehavior {
  protected direction: string;

  startUp(): void {
    this.direction = this.options.direction;
    this.entityBody.setAllowGravity(true);
    this.entityBody.setBounce(1, 0);
    this.entityControlMatrix.data.moveLeft = this.direction === Direction.LEFT;
    this.entityControlMatrix.data.moveRight = this.direction === Direction.RIGHT;
  }
}
