'use strict';

import Sprite from '../../components/sprite';
import TweenQueue from '../../components/tween';
import FloatStillBehavior from './float-still';

export default class FlyStillBehavior extends FloatStillBehavior {
  startUp(): void {
    super.startUp();
    this.F.entityManager.addComponentToEntity(this.entity, TweenQueue, { items: [
      {
        componentType: Sprite,
        stateTweenTargetKey: 'sprite',
        properties: { y: this.entityBody.center.y + this.options.waveAmplitude * this.F.config.size.SCALE },
        duration: this.options.waveSpeed,
        ease: 'Linear',
        loop: true,
        yoyo: true,
        permanent: true
      }
    ]});
  }
}
