'use strict';

import FlyStillBehavior from './fly-still';

export default class FlyTowardsHeroHorizontalBehavior extends FlyStillBehavior {
  startUp(): void {
    super.startUp();
    this.entityBody.setBounce(1, 0);
  }

  apply(): void {
    if (this.getDistanceBetweenEntityAndHero() < this.entityBody.width / 2) {
      this.entityControlMatrix.data.stop = true;
    }
    else if (this.entityBody.center.x < this.heroBody.center.x) {
      this.entityControlMatrix.data.moveRight = true;
    }
    else {
      this.entityControlMatrix.data.moveLeft = true;
    }
  }
}
