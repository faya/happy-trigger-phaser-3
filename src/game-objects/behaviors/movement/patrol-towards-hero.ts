'use strict';

import BaseBehavior from '../behavior';

export default class PatrolTowardsHeroBehavior extends BaseBehavior {
  startUp(): void {
    this.entityBody.setAllowGravity(true);
    this.entityBody.setBounce(1, 0);
  }

  apply(): void {
    if (this.getDistanceBetweenEntityAndHero() < this.entityBody.width / 2) {
      this.entityControlMatrix.data.stop = true;
    }
    else if (this.entityBody.center.x < this.heroBody.center.x) {
      this.entityControlMatrix.data.moveRight = true;
    }
    else {
      this.entityControlMatrix.data.moveLeft = true;
    }
  }
}
