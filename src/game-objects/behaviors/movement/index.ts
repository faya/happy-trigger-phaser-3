'use strict';

import FloatStillBehavior from './float-still';
import FlyPatrolBehavior from './fly-patrol';
import FlyStillBehavior from './fly-still';
import FlyTowardsHeroHorizontalBehavior from './fly-towards-hero-horizontal';
import PatrolBlindBehavior from './patrol-blind';
import PatrolJumpBehavior from './patrol-jump';
import PatrolTowardsHeroBehavior from './patrol-towards-hero';
import StandStillBehavior from './stand-still';

export const MOVEMENT_BEHAVIORS_KEYS = {
  STAND_STILL: 'STAND_STILL',
  FLOAT_STILL: 'FLOAT_STILL',
  PATROL_BLIND: 'PATROL_BLIND',
  PATROL_JUMP: 'PATROL_JUMP',
  PATROL_TOWARDS_HERO: 'PATROL_TOWARDS_HERO',
  FLY_STILL: 'FLY_STILL',
  FLY_PATROL: 'FLY_PATROL',
  FLY_TOWARDS_HERO_HORIZONTAL: 'FLY_TOWARDS_HERO_HORIZONTAL',
};

export const MOVEMENT_BEHAVIORS = {
  [MOVEMENT_BEHAVIORS_KEYS.STAND_STILL]: StandStillBehavior,
  [MOVEMENT_BEHAVIORS_KEYS.PATROL_BLIND]: PatrolBlindBehavior,
  [MOVEMENT_BEHAVIORS_KEYS.PATROL_TOWARDS_HERO]: PatrolTowardsHeroBehavior,
  [MOVEMENT_BEHAVIORS_KEYS.PATROL_JUMP]: PatrolJumpBehavior,
  [MOVEMENT_BEHAVIORS_KEYS.FLOAT_STILL]: FloatStillBehavior,
  [MOVEMENT_BEHAVIORS_KEYS.FLY_STILL]: FlyStillBehavior,
  [MOVEMENT_BEHAVIORS_KEYS.FLY_PATROL]: FlyPatrolBehavior,
  [MOVEMENT_BEHAVIORS_KEYS.FLY_TOWARDS_HERO_HORIZONTAL]: FlyTowardsHeroHorizontalBehavior
};
