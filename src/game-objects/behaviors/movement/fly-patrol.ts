'use strict';

import { Direction } from '../../misc/direction';
import FlyStillBehavior from './fly-still';

export default class FlyPatrolBehavior extends FlyStillBehavior {
  startUp(): void {
    super.startUp();
    this.entityBody.setBounce(1, 0);
    this.entityControlMatrix.data.moveLeft = this.options.direction === Direction.LEFT;
    this.entityControlMatrix.data.moveRight = this.options.direction === Direction.RIGHT;
  }
}
