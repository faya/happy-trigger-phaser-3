'use strict';

import BaseBehavior from '../behavior';

export default class FloatStillBehavior extends BaseBehavior {
  startUp(): void {
    this.entityBody.setAllowGravity(false);
  }
}
