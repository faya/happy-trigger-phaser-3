'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import SystemRoutine from '../../services/ecs/systems/system-routine';
import Facade from '../../services/facade/facade';
import ControllableByMatrix from '../components/controllable-by-matrix';
import HeroSprite from '../components/hero-sprite';
import Sprite from '../components/sprite';

export default abstract class BaseBehavior extends SystemRoutine {
  constructor(
    protected scene: Phaser.Scene,
    protected F: Facade,
    public entity: Entity,
    public hero: Entity,
    protected options: any) {
    super(scene, F, options);
  }

  protected get heroBody(): Phaser.Physics.Arcade.Body {
    return this.F.entityManager.getComponentFromEntity(this.hero, HeroSprite).state.container.body;
  }

  protected get entityBody(): Phaser.Physics.Arcade.Body {
    return this.F.entityManager.getComponentFromEntity(this.entity, Sprite).state.sprite.body;
  }

  protected get entityControlMatrix(): Component<ControllableByMatrix> {
    return this.F.entityManager.getComponentFromEntity(this.entity, ControllableByMatrix);
  }

  protected getAngleBetweenEntityAndHero(): number {
    return Phaser.Math.RadToDeg(Math.atan2(this.heroBody.center.y - this.entityBody.center.y,
      this.heroBody.center.x - this.entityBody.center.x));
  }

  protected getDistanceBetweenEntityAndHero(): number {
    return Math.abs(this.entityBody.center.x - this.heroBody.center.x);
  }
}
