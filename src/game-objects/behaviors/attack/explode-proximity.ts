'use strict';

import Component from '../../../services/ecs/components/component';
import GAME_EVENTS from '../../../services/subject/events/game-events';
import WAVE_EVENTS from '../../../services/subject/events/wave-events';
import Explosion from '../../components/explosion';
import Health from '../../components/health';
import Position from '../../components/position';
import BaseBehavior from '../behavior';

export default class ExplodeProximityBehavior extends BaseBehavior {
  private exploded: boolean = false;

  apply(): void {
    const distanceToHero = Math.hypot(this.heroBody.center.x - this.entityBody.center.x,
      this.heroBody.center.y - this.entityBody.center.y);
    if (!this.exploded && distanceToHero < this.options.range * this.F.config.size.SCALE) {
      // kill ennemy
      const health: Component<Health> = this.F.entityManager.getComponentFromEntity(this.entity, Health);
      this.F.subjectService.dispatch(GAME_EVENTS.ENTITY_DAMAGED, this.entity, health.data.max);

      // create explosion
      const explosion = this.F.entityManager.createEntity('explosion');
      this.F.entityManager.addComponentToEntity(explosion, Position,
        { x: this.entityBody.center.x, y: this.entityBody.center.y });
      this.F.entityManager.addComponentToEntity(explosion, Explosion, {
        radius: 150,
        color:0xFF0000,
        damage: this.options.damage
      });
      this.exploded = true;
    }
  }
}
