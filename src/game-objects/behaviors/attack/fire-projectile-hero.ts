'use strict';

import Component from '../../../services/ecs/components/component';
import Shooting from '../../components/shooting';
import FireProjectileDirectionBehavior from './fire-projectile-direction';

export default class FireProjectileHeroBehavior extends FireProjectileDirectionBehavior {
  apply(): void {
    const shooting: Component<Shooting> = this.F.entityManager.getComponentFromEntity(this.entity, Shooting);
    shooting.data.angle = this.getAngleBetweenEntityAndHero();
    shooting.data.origin = [this.entityBody.center.x, this.entityBody.center.y];
  }
}
