'use strict';

import BaseBehavior from '../behavior';

export default class DoNothingBehavior extends BaseBehavior {}
