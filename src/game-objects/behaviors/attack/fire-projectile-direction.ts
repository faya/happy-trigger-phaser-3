'use strict';

import Component from '../../../services/ecs/components/component';
import Shooting from '../../components/shooting';
import { Direction } from '../../misc/direction';
import BaseBehavior from '../behavior';

export default class FireProjectileDirectionBehavior extends BaseBehavior {
  startUp(): void {
    this.F.entityManager.addComponentToEntity(this.entity, Shooting, {
      projectileSpriteKey: 'projectiles',
      projectileSpriteFrame: 1,
      projectileVelocity: this.options.velocity,
      targetFilters: { collectionNames: ['hero'] },
      damage: this.options.damage,
      origin: [0, 0],
      fireRate: this.options.fireRate,
      angle: 0
    });
  }

  apply(): void {
    const shooting: Component<Shooting> = this.F.entityManager.getComponentFromEntity(this.entity, Shooting);
    shooting.data.angle = Direction.getAngle(this.options.direction);
    shooting.data.origin = [this.entityBody.center.x, this.entityBody.center.y];
  }
}
