'use strict';

import DoNothingBehavior from './do-nothing';
import ExplodeProximityBehavior from './explode-proximity';
import ExplodeTimerBehavior from './explode-timer';
import FireProjectileDirectionBehavior from './fire-projectile-direction';
import FireProjectileHeroBehavior from './fire-projectile-hero';

export const ATTACK_BEHAVIORS_KEYS = {
  DO_NOTHING: 'DO_NOTHING',
  FIRE_PROJECTILE_DIRECTION: 'FIRE_PROJECTILE_DIRECTION',
  FIRE_PROJECTILE_HERO: 'FIRE_PROJECTILE_HERO',
  EXPLODE_PROXIMITY: 'EXPLODE_PROXIMITY',
  EXPLODE_TIMER: 'EXPLODE_TIMER'
};

export const ATTACK_BEHAVIORS = {
  [ATTACK_BEHAVIORS_KEYS.DO_NOTHING]: DoNothingBehavior,
  [ATTACK_BEHAVIORS_KEYS.FIRE_PROJECTILE_DIRECTION]: FireProjectileDirectionBehavior,
  [ATTACK_BEHAVIORS_KEYS.FIRE_PROJECTILE_HERO]: FireProjectileHeroBehavior,
  [ATTACK_BEHAVIORS_KEYS.EXPLODE_TIMER]: ExplodeTimerBehavior,
  [ATTACK_BEHAVIORS_KEYS.EXPLODE_PROXIMITY]: ExplodeProximityBehavior
};
