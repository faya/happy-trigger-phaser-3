'use strict';

import { EnnemyData } from '../enemies/ennemy-data';

export interface WaveData {
  spawns: SpawnData[];
  timeLimit?: number;
}

export interface SpawnData {
  spawnRate: number;
  position: [number, number];
  groups: EnnemyGroupData[];
}

export interface EnnemyGroupData {
  ennemy: EnnemyData;
  number: number;
  movementOptions: any;
  attackOptions: any;
}
