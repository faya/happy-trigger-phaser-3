'use strict';

import { WAVE_1 } from './data/wave-1';
import { WAVE_10 } from './data/wave-10';
import { WAVE_11 } from './data/wave-11';
import { WAVE_12 } from './data/wave-12';
import { WAVE_13 } from './data/wave-13';
import { WAVE_2 } from './data/wave-2';
import { WAVE_3 } from './data/wave-3';
import { WAVE_4 } from './data/wave-4';
import { WAVE_5 } from './data/wave-5';
import { WAVE_6 } from './data/wave-6';
import { WAVE_7 } from './data/wave-7';
import { WAVE_8 } from './data/wave-8';
import { WAVE_9 } from './data/wave-9';
import { WaveData } from './wave-interfaces';

const WAVE_DATA: WaveData[] = [
  WAVE_1,
  WAVE_2,
  WAVE_3,
  WAVE_4,
  WAVE_5,
  WAVE_6,
  WAVE_7,
  WAVE_8,
  WAVE_9,
  WAVE_10,
  WAVE_11,
  WAVE_12,
  WAVE_13
];

export default WAVE_DATA;
