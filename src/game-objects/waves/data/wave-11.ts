'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_11: WaveData = {
  spawns: [
    {
      spawnRate: 3000,
      position: [2, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.ZOMBIE_GREEN],
          number: 6,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 4000,
      position: [18, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.ZOMBIE_RED],
          number: 4,
          movementOptions: { direction: Direction.LEFT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 1000,
      position: [6, 3],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.FLY_GRAY],
          number: 1,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 1000,
      position: [14, 3],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.FLY_GRAY],
          number: 1,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    }
  ]
};
