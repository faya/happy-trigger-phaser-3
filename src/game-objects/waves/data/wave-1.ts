'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_1: WaveData = {
  spawns: [
    {
      spawnRate: 3000,
      position: [5, 5],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BLOB_PINK],
          number: 5,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 3000,
      position: [15, 5],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BLOB_PINK],
          number: 5,
          movementOptions: { direction: Direction.LEFT },
          attackOptions: {}
        }
      ]
    }
  ]
};
