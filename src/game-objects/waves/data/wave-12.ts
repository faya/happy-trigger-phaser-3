'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_12: WaveData = {
  spawns: [
    {
      spawnRate: 10000,
      position: [5, 12],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BLOB_GREEN_SPIKY],
          number: 2,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 10000,
      position: [15, 12],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BLOB_GREEN_SPIKY],
          number: 2,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 10000,
      position: [10, 12],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BLOB_GRAY_SPIKY],
          number: 2,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 6000,
      position: [14, 5],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.CYCLOP_BLUE],
          number: 3,
          movementOptions: { direction: Direction.LEFT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 5000,
      position: [2, 2],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BIRD_BLUE],
          number: 3,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    }
  ]
};
