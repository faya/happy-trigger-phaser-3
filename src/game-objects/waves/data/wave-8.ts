'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_8: WaveData = {
  spawns: [
    {
      spawnRate: 4000,
      position: [2, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.PEA_GREEN],
          number: 5,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 6000,
      position: [18, 2],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.FLY_RED],
          number: 3,
          movementOptions: {},
          attackOptions: { direction: Direction.DOWN }
        }
      ]
    }
  ]
};
