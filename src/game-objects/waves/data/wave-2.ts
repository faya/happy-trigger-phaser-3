'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_2: WaveData = {
  spawns: [
    {
      spawnRate: 2000,
      position: [1, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BLOB_PINK],
          number: 6,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 2000,
      position: [19, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BLOB_PINK],
          number: 6,
          movementOptions: { direction: Direction.LEFT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 3000,
      position: [10, 2],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.CYCLOP_BLUE],
          number: 4,
          movementOptions: { direction: Direction.LEFT },
          attackOptions: {}
        }
      ]
    }
  ]
};
