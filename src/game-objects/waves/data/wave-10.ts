'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_10: WaveData = {
  spawns: [
    {
      spawnRate: 3000,
      position: [10, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.CYCLOP_ORANGE],
          number: 4,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 6000,
      position: [6, 5],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.HORN_GREEN],
          number: 3,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 3000,
      position: [14, 5],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.HORN_BLUE],
          number: 6,
          movementOptions: { direction: Direction.LEFT },
          attackOptions: {}
        }
      ]
    }
  ]
};
