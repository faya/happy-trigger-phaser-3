'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_4: WaveData = {
  spawns: [
    {
      spawnRate: 3000,
      position: [10, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.CYCLOP_BLUE],
          number: 5,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 3000,
      position: [2, 2],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.FLY_GREEN],
          number: 1,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 6000,
      position: [10, 2],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.FLY_GREEN],
          number: 1,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 9000,
      position: [18, 2],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.FLY_GREEN],
          number: 1,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    }
  ]
};
