'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_5: WaveData = {
  spawns: [
    {
      spawnRate: 6000,
      position: [18, 2],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BIRD_BLUE],
          number: 5,
          movementOptions: { direction: Direction.LEFT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 5000,
      position: [2, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.ZOMBIE_RED],
          number: 5,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    }
  ]
};
