'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_13: WaveData = {
  spawns: [
    {
      spawnRate: 20000,
      position: [2, 8],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.FLY_GRAY],
          number: 2,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 20000,
      position: [18, 8],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.FLY_GRAY],
          number: 2,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 10000,
      position: [10, 11],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.ROCK_SPIKY],
          number: 2,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 3000,
      position: [2, 2],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BIRD_GREEN],
          number: 8,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 5000,
      position: [14, 5],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.HORN_BLUE],
          number: 5,
          movementOptions: { direction: Direction.LEFT },
          attackOptions: {}
        }
      ]
    }
  ]
};
