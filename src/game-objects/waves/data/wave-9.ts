'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_9: WaveData = {
  spawns: [
    {
      spawnRate: 6000,
      position: [2, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BLOB_ORANGE],
          number: 4,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 3000,
      position: [18, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BLOB_GREEN],
          number: 8,
          movementOptions: { direction: Direction.LEFT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 5000,
      position: [6, 5],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.ROCK_SPIKY],
          number: 1,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 5000,
      position: [14, 5],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.ROCK_SPIKY],
          number: 1,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    }
  ]
};
