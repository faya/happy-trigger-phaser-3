'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_7: WaveData = {
  spawns: [
    {
      spawnRate: 6000,
      position: [2, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.ZOMBIE_GREEN],
          number: 5,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 5000,
      position: [6, 5],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.ROCK_SPIKY],
          number: 1,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 5000,
      position: [14, 5],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.ROCK_SPIKY],
          number: 1,
          movementOptions: {},
          attackOptions: {}
        }
      ]
    }
  ]
};
