'use strict';

import ENNEMY_DATA, { ENNEMY_KEYS } from '../../enemies/ennemy-data';
import { Direction } from '../../misc/direction';
import { WaveData } from '../wave-interfaces';

export const WAVE_6: WaveData = {
  spawns: [
    {
      spawnRate: 6000,
      position: [18, 2],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.BIRD_GREEN],
          number: 3,
          movementOptions: { direction: Direction.LEFT },
          attackOptions: {}
        }
      ]
    },
    {
      spawnRate: 5000,
      position: [2, 13],
      groups: [
        {
          ennemy: ENNEMY_DATA[ENNEMY_KEYS.HORN_GREEN],
          number: 6,
          movementOptions: { direction: Direction.RIGHT },
          attackOptions: {}
        }
      ]
    }
  ]
};
