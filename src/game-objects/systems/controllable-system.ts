'use strict';

import Component from '../../services/ecs/components/component';
import System from '../../services/ecs/systems/system';
import BaseControllable from '../components/controllable';
import State from '../components/state';
import { Direction } from '../misc/direction';

export default abstract class BaseControllableSystem extends System {
  protected switchDirection(body: Phaser.Physics.Arcade.Body, velocity: number, newDirection: string): void {
    if (Direction.getDirectionsFromVelocity(body.velocity.x, body.velocity.y)[newDirection]) {
      // already in that direction
      return;
    }

    body.setVelocityX(velocity * this.F.config.size.SCALE * Direction.getDirectionFactor(newDirection));
  }

  protected jump(
    body: Phaser.Physics.Arcade.Body,
    control: Component<BaseControllable>,
    state: Component<State>
  ): void {
    if (state.state.jumpCharges <= 0) {
      return;
    }
    state.state.jumpCharges--;
    state.state.isTouchingGround = false;
    body.setVelocityY(-1 * control.data.jumpVelocity * this.F.config.size.SCALE);
  }
}
