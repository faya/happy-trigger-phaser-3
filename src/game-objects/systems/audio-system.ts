'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import HERO_EVENTS from '../../services/subject/events/hero-events';
import SCENE_EVENTS from '../../services/subject/events/scene-events';
import { Audio, BackgroundAudio, OnEventAudio, OnEventMultipleAudio } from '../components/audio';

export default class AudioSystem extends System {
  private backgroundAudio: Phaser.Sound.BaseSound;

  protected get backgroundAudioEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [BackgroundAudio] });
  }

  protected get onEventAudioEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [OnEventAudio] });
  }

  protected get onEventMultipleAudioEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [OnEventMultipleAudio] });
  }

  create(): void {
    this.F.subjectService.subscribe(SCENE_EVENTS.SCENE_CHANGED, this.stopBackgroundAudio.bind(this));
    this.F.subjectService.subscribe(HERO_EVENTS.DIED, this.stopBackgroundAudio.bind(this));
  }

  update(): void {
    for (const entity of this.backgroundAudioEntities) {
      const audio = this.getComponent(entity, BackgroundAudio);
      this.playBackgroundAudio(audio);
    }

    for (const entity of this.onEventAudioEntities) {
      const audio = this.getComponent(entity, OnEventAudio);
      if (audio.state.initialized !== true) {
        audio.state.initialized = true;
        this.manageOnEventAudio(this.getComponent(entity, OnEventAudio).data);
      }
    }

    for (const entity of this.onEventMultipleAudioEntities) {
      const audioCollection = this.getComponent(entity, OnEventMultipleAudio);
      if (audioCollection.state.initialized !== true) {
        audioCollection.state.initialized = true;
        for (const audio of audioCollection.data.items) {
          this.manageOnEventAudio(audio);
        }
      }
    }
  }

  private playBackgroundAudio(audio: Component<BackgroundAudio>): void {
    if (audio.data.key !== audio.state.current) {
      if (this.backgroundAudio !== undefined) {
        this.backgroundAudio.destroy();
      }
      audio.state.current = audio.data.key;
      this.backgroundAudio = this.createAudio(audio.data);
      (this.backgroundAudio as any).loop = true;
      this.backgroundAudio.play();
    }
  }

  private manageOnEventAudio(audio: OnEventAudio): void {
    const instance = this.createAudio(audio);
    for (const event of audio.events) {
      this.F.subjectService.subscribe(event, this.playAudio.bind(this, instance));
    }
  }

  playAudio(audio: Phaser.Sound.BaseSound): void {
    audio.play();
  }

  private createAudio(audioData: Audio): Phaser.Sound.BaseSound {
    return this.scene.sound.add(audioData.key, { volume: audioData.volume });
  }

  private stopBackgroundAudio(): void {
    if (this.backgroundAudio !== undefined && this.backgroundAudio.isPlaying) {
      this.backgroundAudio.destroy();
    }
  }
}
