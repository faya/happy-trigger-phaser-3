'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import BitmapText from '../components/bitmap-text';
import Position from '../components/position';

export default class BitmapTextSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [BitmapText, Position] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const text = this.getComponent(entity, BitmapText);
      if (text.state.text === undefined) {
        this.createText(text, this.getComponent(entity, Position));
      }
      this.updateText(text);
    }
  }

  private createText(bitmapText: Component<BitmapText>, position: Component<Position>): void {
    bitmapText.state.text = this.scene.add.bitmapText(
      position.data.x,
      position.data.y,
      bitmapText.data.font,
      bitmapText.data.text,
      bitmapText.data.size * this.F.config.size.SCALE
    );

    const originToApply = bitmapText.data.origin !== undefined ? bitmapText.data.origin : [0.5, 0.5];
    bitmapText.state.text.setOrigin(originToApply[0], originToApply[1]);

    if (bitmapText.data.align !== undefined) {
      bitmapText.state.text.align = bitmapText.data.align;
    }
  }

  private updateText(bitmapText: Component<BitmapText>): void {
    if (bitmapText.data.text !== bitmapText.state.text.text) {
      bitmapText.state.text.setText(bitmapText.data.text);
    }
    if (bitmapText.data.hidden !== undefined) {
      bitmapText.state.text.setVisible(!bitmapText.data.hidden);
    }
  }
}
