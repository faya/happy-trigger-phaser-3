'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import ControllableByEvents from '../components/controllable-by-events';
import State from '../components/state';
import { Direction } from '../misc/direction';
import BaseControllableSystem from './controllable-system';

export default class ControllableByEventsSystem extends BaseControllableSystem {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [ControllableByEvents] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const control: Component<ControllableByEvents> = this.getComponent(entity, ControllableByEvents);
      if (control.state.initialized !== true) {
        const body: Phaser.Physics.Arcade.Body =
          this.getComponent(entity, control.data.componentType).state[control.data.stateBodyKey].body;
        const state: Component<State> = this.getComponent(entity, State);
        this.listenControlEvents(control, state, body);
        control.state.initialized = true;
      }
    }
  }

  private listenControlEvents(
    control: Component<ControllableByEvents>,
    state: Component<State>,
    body: Phaser.Physics.Arcade.Body
  ): void {
    this.F.subjectService.subscribe(control.data.leftEvent,
      this.switchDirection.bind(this, body, control.data.horizontalVelocity, Direction.LEFT));
    this.F.subjectService.subscribe(control.data.rightEvent,
      this.switchDirection.bind(this, body, control.data.horizontalVelocity, Direction.RIGHT));
    this.F.subjectService.subscribe(control.data.jumpEvent, this.jump.bind(this, body, control, state));
  }
}
