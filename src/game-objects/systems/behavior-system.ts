'use strict';

import BehaviorFactory from '../../scenes/game/ennemy/behavior-factory';
import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import BaseBehavior from '../behaviors/behavior';
import BehaviorCollection from '../components/behavior-collection';
import ControllableByMatrix from '../components/controllable-by-matrix';
import { BehaviorData } from '../enemies/ennemy-data';

export default class BehaviorSystem extends System {
  private behaviorFactory: BehaviorFactory;
  private hero: Entity;

  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [BehaviorCollection, ControllableByMatrix] });
  }

  create(): void {
    this.behaviorFactory = new BehaviorFactory(this.scene, this.F);
    this.hero = this.F.entityManager.getEntities({ tags: ['hero'] })[0];
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const collection: Component<BehaviorCollection> = this.getComponent(entity, BehaviorCollection);
      collection.state.initialized = collection.state.initialized !== undefined ? collection.state.initialized : [];
      collection.state.activeBehaviors = collection.state.activeBehaviors !== undefined ?
        collection.state.activeBehaviors : [];
      for (const behavior of collection.data.items) {
        if (!collection.state.initialized.includes(behavior[1].key)) {
          this.initializeBehavior(entity, behavior, collection);
        }
      }
      this.applyBehavior(collection.state.activeBehaviors);
    }
  }

  private initializeBehavior(
    entity: Entity,
    data: [string, BehaviorData, any],
    collection: Component<BehaviorCollection>
  ): void {
    collection.state.initialized.push(data[1].key);
    const behavior = this.behaviorFactory.build(data[0], data[1], data[2], entity, this.hero);
    behavior.startUp();
    collection.state.activeBehaviors.push(behavior);
  }

  private applyBehavior(activeBehaviors: BaseBehavior[]): void {
    for (const behavior of activeBehaviors) {
      behavior.apply();
    }
  }
}
