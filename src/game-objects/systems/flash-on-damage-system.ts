'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import GAME_EVENTS from '../../services/subject/events/game-events';
import Health from '../components/health';
import Sprite from '../components/sprite';
import { Direction } from '../misc/direction';

export default class FlashOnDamageSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [Health, Sprite] });
  }

  create(): void {
    this.F.subjectService.subscribe(GAME_EVENTS.ENTITY_DAMAGED, this.flash.bind(this));
  }

  private flash(entity: Entity): void {
    if (this.impactedEntities.includes(entity)) {
      const sprite: Component<Sprite> = this.getComponent(entity, Sprite);
      const previousTint = (sprite.state.sprite as Phaser.GameObjects.Sprite).tint;
      (sprite.state.sprite as Phaser.GameObjects.Sprite).tint = 0xFF0000;
      setTimeout(() => {
        (sprite.state.sprite as Phaser.GameObjects.Sprite).tint = previousTint;
      }, 100);
    }
  }
}
