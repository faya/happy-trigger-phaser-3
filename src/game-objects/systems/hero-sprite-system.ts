'use strict';

import REGISTRY from '../../registry';
import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import GAME_EVENTS from '../../services/subject/events/game-events';
import HERO_EVENTS from '../../services/subject/events/hero-events';
import WAVE_EVENTS from '../../services/subject/events/wave-events';
import HeroSprite from '../components/hero-sprite';
import Position from '../components/position';
import Shooting from '../components/shooting';
import Sprite from '../components/sprite';
import SpriteAnimation from '../components/sprite-animation';
import State from '../components/state';
import Tween from '../components/tween';
import { Direction } from '../misc/direction';

export default class HeroSpriteSystem extends System {
  private hero: Entity;
  private positionComponent: Component<Position>;
  private spriteComponent: Component<HeroSprite>;
  private controlComponent: Component<State>;
  private shootingComponent: Component<Shooting>;
  private animationComponent: Component<SpriteAnimation>;
  private tweenComponent: Component<Tween>;

  private instanciateEntity(): void {
    this.hero = this.F.entityManager.getEntities({ tags: ['hero'] })[0];
    this.positionComponent = this.getComponent(this.hero, Position);
    this.spriteComponent = this.getComponent(this.hero, HeroSprite);
    this.controlComponent = this.getComponent(this.hero, State);
    this.shootingComponent = this.getComponent(this.hero, Shooting);
    this.animationComponent = this.getComponent(this.hero, SpriteAnimation);
    this.tweenComponent = this.getComponent(this.hero, Tween);

    this.createContainer();
    this.createSprites();
    this.spriteComponent.state.container.setScale(this.F.config.size.SCALE, this.F.config.size.SCALE);

    if (this.shootingComponent.data.onShootEvent !== undefined) {
      this.F.subjectService.subscribe(this.shootingComponent.data.onShootEvent, this.playMuzzleFlash.bind(this));
      this.F.subjectService.subscribe(this.shootingComponent.data.onShootEvent, this.animateHeroWeapon.bind(this));
    }
  }

  create(): void {
    this.F.subjectService.subscribe(GAME_EVENTS.ENTITY_DAMAGED, this.entityDamagedHandler.bind(this));
    this.F.subjectService.subscribe(HERO_EVENTS.DIED, this.heroDeathHandler.bind(this));
    this.F.subjectService.subscribe(WAVE_EVENTS.LAST_WAVE_END, this.stopHero.bind(this));
  }

  update(time: number, delta: number): void {
    if (this.hero === undefined) {
      this.instanciateEntity();
    }

    this.updateWeaponSprite();
    this.updateMuzzlePosition();
    this.updateCrosshairPosition();

    (this.spriteComponent.state.bodySprite as Phaser.GameObjects.Sprite).anims.update(time, delta);
    (this.spriteComponent.state.weaponSprite as Phaser.GameObjects.Sprite).anims.update(time, delta);
  }

  private get aimDirection(): string {
    const angle = (this.spriteComponent.state.weaponSprite.angle + 360) % 360;
    return Direction.getHorizontalDirectionFromAngle(angle);
  }

  private createContainer(): void {
    this.spriteComponent.state.container = new Phaser.GameObjects.Container(
      this.scene,
      this.positionComponent.data.x,
      this.positionComponent.data.y
    );
    this.scene.add.existing(this.spriteComponent.state.container);
    this.spriteComponent.state.container.setSize(this.F.config.size.HERO_WIDTH, this.F.config.size.HERO_HEIGHT);
    this.scene.physics.world.enable(this.spriteComponent.state.container);
    this.spriteComponent.state.container.body.setBounce(1, 0).setCollideWorldBounds(true);
    this.spriteComponent.state.container.body.setVelocity(
      this.F.config.game.HERO_VELOCITY * this.F.config.size.SCALE,
      0
    );
  }

  private createSprites(): void {
    // Weapon arm
    // (arm rotating when aiming, shoots projectiles)
    this.spriteComponent.state.weaponSprite = this.instanciateSprite(
      this.spriteComponent.data.spritesData['weapon'],
      this.spriteComponent.data.spritesPos['weapon']
    );
    this.shootingComponent.data.angle = this.spriteComponent.state.weaponSprite.angle;

    // Body
    this.spriteComponent.state.bodySprite = this.instanciateSprite(
      this.spriteComponent.data.spritesData['body'],
      this.spriteComponent.data.spritesPos['body']
    );

    // Crosshair
    this.spriteComponent.state.crosshairSprite = this.instanciateSprite(
      this.spriteComponent.data.spritesData['crosshair'],
      this.spriteComponent.data.spritesPos['crosshair']
    );

    this.spriteComponent.state.container.add(this.spriteComponent.state.weaponSprite);
    this.spriteComponent.state.container.add(this.spriteComponent.state.bodySprite);
    this.spriteComponent.state.container.add(this.spriteComponent.state.crosshairSprite);
  }

  private instanciateSprite(data: Sprite, pos: [number, number]): Phaser.GameObjects.Sprite {
    const sprite = new Phaser.GameObjects.Sprite(
      this.scene,
      pos[0],
      pos[1],
      data.key,
      data.frame);
    if (data.origin !== undefined) {
      sprite.setOrigin(data.origin[0], data.origin[1]);
    }
    if (data.angle !== undefined) {
      sprite.setAngle(data.angle);
    }
    return sprite;
  }

  private updateWeaponSprite(): void {
    // position
    (this.spriteComponent.state.weaponSprite as Phaser.GameObjects.Sprite)
      .setX(Math.abs(this.spriteComponent.state.weaponSprite.x) * Direction.getDirectionFactor(this.aimDirection));
    // angle
    (this.spriteComponent.state.weaponSprite as Phaser.GameObjects.Sprite).angle = this.shootingComponent.data.angle;
    (this.spriteComponent.state.crosshairSprite as Phaser.GameObjects.Sprite).angle = this.shootingComponent.data.angle;
  }

  private updateMuzzlePosition(): void {
    this.shootingComponent.data.origin = [
      this.spriteComponent.state.container.x + this.spriteComponent.state.weaponSprite.x * this.F.config.size.SCALE
        + this.F.config.size.SCALED_HERO_ARM_LENGTH
        * Math.cos(Phaser.Math.DegToRad(this.spriteComponent.state.weaponSprite.angle)),
      this.spriteComponent.state.container.y + this.spriteComponent.state.weaponSprite.y * this.F.config.size.SCALE
        + this.F.config.size.SCALED_HERO_ARM_LENGTH
        * Math.sin(Phaser.Math.DegToRad(this.spriteComponent.state.weaponSprite.angle))
    ];

    this.shootingComponent.data.angle = this.scene.registry.get(REGISTRY.AIM_ANGLE);
    this.controlComponent.data.aimDirection =
      Direction.getHorizontalDirectionFromAngle(this.shootingComponent.data.angle);
  }

  private updateCrosshairPosition(): void {
    this.spriteComponent.state.crosshairSprite.setX(
      this.F.config.size.SCALE * this.F.config.size.CROSSHAIR_DISTANCE
      * Math.cos(Phaser.Math.DegToRad(this.spriteComponent.state.weaponSprite.angle))
    );
    this.spriteComponent.state.crosshairSprite.setY(
      this.F.config.size.SCALE * this.F.config.size.CROSSHAIR_DISTANCE
      * Math.sin(Phaser.Math.DegToRad(this.spriteComponent.state.weaponSprite.angle))
    );
  }

  private playMuzzleFlash(): void {
    if (this.animationComponent.state.initialized === undefined) {
      return;
    }
    this.spriteComponent.state.weaponSprite.anims.play(`${this.hero.id}|muzzle-flash`);
  }

  private animateHeroWeapon(): void {
    if (this.spriteComponent.state.weaponSprite === undefined) {
      return;
    }
    const weaponSprite = this.spriteComponent.state.weaponSprite;
    this.tweenComponent.data.items.push(Object.assign(new Tween(), {
      componentType: HeroSprite,
      stateTweenTargetKey: 'weaponSprite',
      duration: 50,
      yoyo: true,
      permanent: false,
      ease: 'Linear',
      properties: {
        x: weaponSprite.x - Direction.getDirectionFactor(this.aimDirection) * 5,
        y: weaponSprite.y
      }
    }));
  }

  private entityDamagedHandler(entity: Entity): void {
    if (entity !== this.hero) {
      return;
    }

    // hero has been damaged, flash his sprites
    const previousTint = this.spriteComponent.state.bodySprite.tint;
    this.spriteComponent.state.bodySprite.tint = 0xFF0000;
    this.spriteComponent.state.weaponSprite.tint = 0xFF0000;
    setTimeout(() => {
      this.spriteComponent.state.bodySprite.tint = previousTint;
      this.spriteComponent.state.weaponSprite.tint = previousTint;
    }, 100);
  }

  private heroDeathHandler(): void {
    (this.spriteComponent.state.container.body as Phaser.Physics.Arcade.Body)
      .setVelocity(0, 0)
      .setAllowGravity(false);
    this.scene.tweens.add({
      targets: this.spriteComponent.state.container,
      duration: 1000,
      ease: 'Cubic.easeIn',
      x: this.F.config.size.SCALED_GAME_WIDTH / 2,
      y: this.F.config.size.SCALED_GAME_HEIGHT / 2
    });
    this.scene.tweens.add({
      targets: this.spriteComponent.state.container,
      duration: 500,
      delay: 750,
      ease: 'Linear',
      angle: 360,
      scaleX: 0,
      scaleY: 0
    });
  }

  private stopHero(): void {
    this.scene.tweens.add({
      targets: this.spriteComponent.state.container.body.velocity,
      duration: 500,
      ease: 'Linear',
      x: 0,
      y: 0
    });
  }
}
