'use strict';

import REGISTRY from '../../registry';
import EnnemyFactory from '../../scenes/game/ennemy/ennemy-factory';
import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import WAVE_EVENTS from '../../services/subject/events/wave-events';
import { EVENT_PRIORITIES } from '../../services/subject/subject-service';
import Position from '../components/position';
import Spawn from '../components/spawn';
import Sprite from '../components/sprite';
import TweenQueue from '../components/tween';

export default class SpawnSystem extends System {
  private ennemyFactory: EnnemyFactory;

  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [Spawn] });
  }

  create(): void {
    this.ennemyFactory = new EnnemyFactory(this.F);
    this.F.subjectService
      .subscribe(WAVE_EVENTS.ENNEMY_DIED, this.decreaseEnnemyCount.bind(this), EVENT_PRIORITIES.HIGH);
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const spawnComponent: Component<Spawn> = this.getComponent(entity, Spawn);
      if (spawnComponent.state.initialized !== true) {
        spawnComponent.state.initialized = true;
        this.prepareEnnemyPool(spawnComponent);
      }
      this.trySpawnEnnemy(spawnComponent);
    }
  }

  private prepareEnnemyPool(spawnComponent: Component<Spawn>): void {
    spawnComponent.state.ennemyPool = [];
    for (const group of spawnComponent.data.config.groups) {
      const movementOptions = Object.assign({},
        JSON.parse(JSON.stringify(group.ennemy.movementBehavior.options)),
        JSON.parse(JSON.stringify(group.movementOptions)));
      group.ennemy.movementBehavior.options = movementOptions;

      const attackOptions = Object.assign({},
        JSON.parse(JSON.stringify(group.ennemy.attackBehavior.options)),
        JSON.parse(JSON.stringify(group.attackOptions)));
      group.ennemy.attackBehavior.options = attackOptions;

      for (let i = 0; i < group.number; i++) {
        spawnComponent.state.ennemyPool.push(Object.assign({},
          JSON.parse(JSON.stringify(group.ennemy))));
      }
      Object.assign(spawnComponent.state, {
        get isEnnemyPoolEmpty(): boolean {
          return (this as any).ennemyPool !== undefined
           && (this as any).ennemyPool.length === 0;
        }
      });
    }
  }

  private trySpawnEnnemy(spawnComponent: Component<Spawn>): void {
    const currentTick = this.scene.time.now;
    if (spawnComponent.state.ennemyPool.length === 0
      || spawnComponent.state.lastSpawnTick + spawnComponent.data.config.spawnRate > currentTick) {
      return;
    }
    spawnComponent.state.lastSpawnTick = currentTick;

    // A new ennemy will spawn
    // display spawn visual queue
    const spawnCue = this.F.entityManager.createEntity('spawn');
    this.F.entityManager.addComponentToEntity(spawnCue, Position, {
      x: spawnComponent.data.config.position[0] * this.F.config.size.SCALED_TILE_SIZE,
      y : spawnComponent.data.config.position[1] * this.F.config.size.SCALED_TILE_SIZE
    });
    this.F.entityManager.addComponentToEntity(spawnCue, Sprite, { key: 'spawn-cue' });
    this.F.entityManager.addComponentToEntity(spawnCue, TweenQueue, { items: [{
      componentType: Sprite,
      stateTweenTargetKey: 'sprite',
      duration: 1500,
      permanent: false,
      ease: 'Back.easeOut',
      properties: { angle: 360, scaleX: 0, scaleY: 0 }
    }] });
    // concrete spawn of ennemy happens 1.5s after visual cue has been shown
    setTimeout(() => {
      const ennemyToSpawnData = spawnComponent.state.ennemyPool.shift();
      const ennemy = this.ennemyFactory
        .build(ennemyToSpawnData, spawnComponent.data.config.position[0], spawnComponent.data.config.position[1]);
      this.ennemyFactory.buildBehaviors(ennemy, ennemyToSpawnData, {}, {});
      this.scene.registry
        .set(REGISTRY.NB_REMAINING_ENNEMIES, this.scene.registry.get(REGISTRY.NB_REMAINING_ENNEMIES) + 1);
    }, 500);

  }

  private decreaseEnnemyCount(): void {
    this.scene.registry
      .set(REGISTRY.NB_REMAINING_ENNEMIES, this.scene.registry.get(REGISTRY.NB_REMAINING_ENNEMIES) - 1);
  }
}
