'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import GAME_EVENTS from '../../services/subject/events/game-events';
import HERO_EVENTS from '../../services/subject/events/hero-events';
import Explosion from '../components/explosion';
import HeroSprite from '../components/hero-sprite';
import Position from '../components/position';

const EXPLOSION_DELAY: number = 100;

export default class ExplosionSystem extends System {
  private hero: Entity;

  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ collectionNames: ['explosion'] });
  }

  create(): void {
    this.F.entityManager.createCollection('explosion');
    this.hero = this.F.entityManager.getEntities({ collectionNames: ['hero'] })[0];
  }

  update(): void {
    const finishedIds: string[] = [];
    for (const entity of this.impactedEntities) {
      const explosion: Component<Explosion> = this.getComponent(entity, Explosion);
      const position: Component<Position> = this.getComponent(entity, Position);
      if (explosion.state.initialized !== true) {
        this.explode(entity.id, explosion, position);
      }
      const finished: boolean = this.updateExplosion(entity.id, explosion, position);
      if (finished) {
        finishedIds.push(entity.id);
      }
    }
    if (finishedIds.length > 0) {
      this.F.entityManager.destroyEntities({ ids: finishedIds });
    }
  }

  private explode(
    id: string,
    explosion: Component<Explosion>,
    position: Component<Position>
  ): void {
    explosion.state.initialized = true;

    // if hero is in explosion radius, damage him
    const heroBody: Phaser.Physics.Arcade.Body =
      this.F.entityManager.getComponentFromEntity(this.hero, HeroSprite).state.container.body;
    if (explosion.data.damage !== undefined
      && Math.sqrt(Math.pow(heroBody.center.x - position.data.x, 2)
      + Math.pow(heroBody.center.y - position.data.y, 2)) < explosion.data.radius) {
      this.F.subjectService.dispatch(HERO_EVENTS.DAMAGED, explosion.data.damage);
    }

    explosion.state.graphics = this.scene.add.graphics();
    (explosion.state.graphics as Phaser.GameObjects.Graphics).fillStyle(0x000000, 1);
    (explosion.state.graphics as Phaser.GameObjects.Graphics)
      .fillCircle(position.data.x, position.data.y, explosion.data.radius * this.F.config.size.SCALE);
    this.scene.cameras.main.shake(100, 0.025);
    explosion.state.creationTick = this.scene.time.now;

    this.F.subjectService.dispatch(GAME_EVENTS.EXPLOSION);
  }

  private updateExplosion(id: string, explosion: Component<Explosion>, position: Component<Position>): boolean {
    if (this.scene.time.now >= explosion.state.creationTick + 2 * EXPLOSION_DELAY) {
      (explosion.state.graphics as Phaser.GameObjects.Graphics).destroy();
      return true;
    }
    if (explosion.state.update !== true && this.scene.time.now >= explosion.state.creationTick + EXPLOSION_DELAY) {
      explosion.state.updated = true;
      const color = explosion.data.color !== undefined ? explosion.data.color : 0xFFFFFF;
      (explosion.state.graphics as Phaser.GameObjects.Graphics).fillStyle(color, 1);
      (explosion.state.graphics as Phaser.GameObjects.Graphics)
        .fillCircle(position.data.x, position.data.y, explosion.data.radius * this.F.config.size.SCALE);
    }
    return false;
  }
}
