'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import Collidable, { ICollideWithData } from '../components/collidable';
import State from '../components/state';

export default class CollidableSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [Collidable, State] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const collidable: Component<Collidable> = this.getComponent(entity, Collidable);
      if (collidable.state.collidedIds === undefined) {
        collidable.state.collidedIds = [];
      }
      for (const obstacleData of collidable.data.collidesWith) {
        const obstacles = this.F.entityManager.getEntities(obstacleData.filters);
        const obstaclesToCollide = obstacles.filter(o => !collidable.state.collidedIds.includes(o.id));
        if (obstaclesToCollide.length > 0) {
          this.collide(entity, collidable, obstacleData, obstaclesToCollide);
        }
      }
    }
  }

  private collide(
    entity: Entity,
    collidable: Component<Collidable>,
    obstacleData: ICollideWithData,
    obstables: Entity[]
  ): void {
    const entityCollidableObject = this.getComponent(entity, collidable.data.componentType)
      .state[collidable.data.stateCollidableKey];
    const obstacleCollidablesObjects = obstables
      .map(o => this.getComponent(o, obstacleData.componentType).state[obstacleData.stateCollidableKey]);
    const entityState: Component<State> = this.getComponent(entity, State);
    if (obstacleData.collisionEvent !== undefined) {
      this.scene.physics.add.collider(entityCollidableObject, obstacleCollidablesObjects, (o1, o2) => {
        this.F.subjectService
          .dispatch((obstacleData.collisionEvent as string), entityState, entityCollidableObject.body, o2.body);
      });
    }
    else {
      this.scene.physics.add.collider(entityCollidableObject, obstacleCollidablesObjects);
    }
    collidable.state.collidedIds = [...collidable.state.collidedIds, ...obstables.map(o => o.id)];
  }
}
