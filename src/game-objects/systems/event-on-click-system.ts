'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import EventOnClick from '../components/event-on-click';

export default class EventOnClickSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [EventOnClick] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const onClick: Component<EventOnClick> = this.getComponent(entity, EventOnClick);
      if (onClick.state.initialized !== true) {
        onClick.state.initialized = true;
        this.listenClick(entity, onClick);
      }
    }
  }

  private listenClick(entity: Entity, onClick: Component<EventOnClick>): void {
    const object = this.F.entityManager.getComponentFromEntity(entity, onClick.data.componentType)
      .state[onClick.data.stateObjectKey];
    object.setInteractive();
    object.once('pointerup', () => {
      this.F.subjectService.dispatch(onClick.data.event);
    }, this);
  }
}
