'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import ControllableByMatrix from '../components/controllable-by-matrix';
import State from '../components/state';
import { Direction } from '../misc/direction';
import BaseControllableSystem from './controllable-system';

export default class ControllableByMatrixSystem extends BaseControllableSystem {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [ControllableByMatrix] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const control: Component<ControllableByMatrix> = this.getComponent(entity, ControllableByMatrix);
      const body: Phaser.Physics.Arcade.Body =
        this.getComponent(entity, control.data.componentType).state[control.data.stateBodyKey].body;
      const state: Component<State> = this.getComponent(entity, State);
      this.reactToControlMatrix(control, state, body);
    }
  }

  private reactToControlMatrix(
    control: Component<ControllableByMatrix>,
    state: Component<State>,
    body: Phaser.Physics.Arcade.Body
  ): void {
    if (control.data.jump === true) {
      this.jump(body, control, state);
      control.data.jump = false;
    }
    else if (control.data.stop === true) {
      body.setVelocityX(0);
      control.data.stop = false;
    }
    else if (control.data.moveLeft === true) {
      this.switchDirection(body, control.data.horizontalVelocity, Direction.LEFT);
      control.data.moveLeft = false;
    }
    else if (control.data.moveRight === true) {
      this.switchDirection(body, control.data.horizontalVelocity, Direction.RIGHT);
      control.data.moveRight = false;
    }
  }
}
