'use strict';

import IGlobalConfig from '../../config';
import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import WAVE_EVENTS from '../../services/subject/events/wave-events';
import Position from '../components/position';
import Sprite from '../components/sprite';

export default class SpriteSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [Position, Sprite] });
  }

  static setupScale(config: IGlobalConfig): void {
    let windowWidth = window.innerWidth
      || document.body.clientWidth;
    let windowHeight = window.innerHeight
      || document.body.clientHeight;
    if (document.documentElement !== null) {
      windowWidth = windowWidth || document.documentElement.clientWidth;
      windowHeight = windowHeight || document.documentElement.clientHeight;
    }

    const currentRatio: number = windowWidth / windowHeight;
    const targetRatio: number = config.size.GAME_WIDTH / config.size.GAME_HEIGHT;
    if (currentRatio < targetRatio) {
      // available space has more height than width to fit the target ratio
      // we need to adapt height to available width
      config.size.SCALE = windowWidth / config.size.GAME_WIDTH;
    }
    else {
      // available space has more width than height to fit the target ratio
      // we need to adapt width to available height
      config.size.SCALE = windowHeight / config.size.GAME_HEIGHT;
    }
  }

  create(): void {
    this.F.subjectService.subscribe(WAVE_EVENTS.ENNEMY_DIED, this.destroySprite.bind(this));
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const sprite = this.getComponent(entity, Sprite);
      if (sprite.state.sprite === undefined) {
        this.createSprite(this.getComponent(entity, Position), sprite);
      }

      if (sprite.data.hidden !== undefined) {
        sprite.state.sprite.setVisible(!sprite.data.hidden);
      }
    }
  }

  private createSprite(position: Component<Position>, sprite: Component<Sprite>): void {
    sprite.state.sprite = new Phaser.GameObjects.Sprite(
      this.scene,
      position.data.x,
      position.data.y,
      sprite.data.key,
      sprite.data.frame
    );
    sprite.state.sprite.setScale(this.F.config.size.SCALE, this.F.config.size.SCALE);

    if (sprite.data.origin !== undefined) {
      sprite.state.sprite.setOrigin(sprite.data.origin[0], sprite.data.origin[1]);
    }
    if (sprite.data.angle !== undefined) {
      sprite.state.sprite.setAngle(sprite.data.angle);
    }
    if (sprite.data.alpha !== undefined && sprite.data.alpha >= 0 && sprite.data.alpha <= 1) {
      sprite.state.sprite.alpha = sprite.data.alpha;
    }
    if (sprite.data.scale !== undefined) {
      sprite.state.sprite.setScale(sprite.data.scale, sprite.data.scale);
    }
    this.scene.add.existing(sprite.state.sprite);
  }

  private destroySprite(entity: Entity): void {
    if (this.impactedEntities.includes(entity)) {
      const sprite: Component<Sprite> = this.getComponent(entity, Sprite);
      (sprite.state.sprite as Phaser.GameObjects.Sprite).destroy();
    }
  }
}
