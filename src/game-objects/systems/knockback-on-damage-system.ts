'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import GAME_EVENTS from '../../services/subject/events/game-events';
import Health from '../components/health';
import Sprite from '../components/sprite';
import { Direction } from '../misc/direction';

export default class KnockBackOnDamageSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [Health, Sprite] });
  }

  create(): void {
    this.F.subjectService.subscribe(GAME_EVENTS.ENTITY_DAMAGED, this.knockback.bind(this));
  }

  private knockback(entity: Entity, damageValue: number, direction: string): void {
    if (this.impactedEntities.includes(entity)) {
      const sprite: Component<Sprite> = this.getComponent(entity, Sprite);
      (sprite.state.sprite as Phaser.GameObjects.Sprite).x += this.F.config.game.KNOCKBACK
        * this.F.config.size.SCALE * Direction.getDirectionFactor(direction);
    }
  }
}
