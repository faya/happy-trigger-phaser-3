'use strict';

import Component, { IComponentData } from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import HeroSprite from '../components/hero-sprite';
import Sprite from '../components/sprite';
import SpriteAnimation from '../components/sprite-animation';

export default class SpriteAnimationSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [SpriteAnimation] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const animation: Component<SpriteAnimation> = this.getComponent(entity, SpriteAnimation);
      const sprite: Phaser.GameObjects.Sprite = this.getSprite(entity, animation);
      if (animation.state.initialized === undefined) {
        this.createAnimations(entity, animation);
        this.animateSprite(sprite, entity.id, animation);
        animation.state.initialized = true;
      }
      this.updateSpriteAnimation(sprite, entity.id, animation);
    }
  }

  private getSprite(
    entity: Entity,
    anim: Component<SpriteAnimation>
  ): Phaser.GameObjects.Sprite {
    const component = this.getComponent(entity, anim.data.componentType);
    return component.state[anim.data.stateSpriteKey];
  }

  private createAnimations(
    entity: Entity,
    anim: Component<SpriteAnimation>
  ): void {
    const component = this.getComponent(entity, anim.data.componentType);

    if (component.data instanceof Sprite && component.data.animations !== undefined) {
      this.createSpriteAnimations(entity.id, component.data.key, component.data.animations);
    }
    else if (component.data instanceof HeroSprite
      && component.data.spritesData.body.animations !== undefined
      && component.data.spritesData.weapon.animations !== undefined) {
      this.createSpriteAnimations(
        entity.id,
        component.data.spritesData.body.key,
        component.data.spritesData.body.animations);
      this.createSpriteAnimations(
        entity.id,
        component.data.spritesData.weapon.key,
        component.data.spritesData.weapon.animations
      );
    }
  }

  private createSpriteAnimations(
    id: string,
    spriteKey: string,
    animations: [string, GenerateFrameNumbersConfig, number, boolean][]
  ): void {
    if (animations !== undefined) {
      for (const animation of animations) {
        if (this.scene.anims.get(`${id}|${animation[0]}`) !== undefined) {
          continue;
        }

        let framesData: AnimationFrameConfig[];
        framesData = this.scene.anims.generateFrameNumbers(
          spriteKey,
          animation[1],
        );

        this.scene.anims.create({
          key: `${id}|${animation[0]}`,
          frames: framesData,
          frameRate: animation[2],
          repeat: animation[3] ? -1 : undefined
        });
      }
    }
  }

  private animateSprite(sprite: Phaser.GameObjects.Sprite, id: string, anim: Component<SpriteAnimation>): void {
    if (anim.data.current === undefined) {
      return;
    }

    // play once
    if (anim.data.repeat === true) {
      // loop animation
      this.scene.anims.get(anim.data.current).repeat = -1;
      if (anim.data.repeatDelay !== undefined) {
        // play animation every anim.data.repeatDelay ms
        this.scene.anims.get(anim.data.current).repeatDelay = anim.data.repeatDelay;
      }
    }
    sprite.anims.play(anim.data.current);
    anim.state.currentAnimKey = anim.data.current;
  }

  private updateSpriteAnimation(sprite: Phaser.GameObjects.Sprite, id: string, anim: Component<SpriteAnimation>): void {
    if (anim.data.current !== undefined && anim.data.current === sprite.anims.currentAnim.key) {
      return;
    }
    this.animateSprite(sprite, id, anim);
  }
}
