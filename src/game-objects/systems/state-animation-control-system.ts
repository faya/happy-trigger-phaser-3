'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import GAME_EVENTS from '../../services/subject/events/game-events';
import SpriteAnimation from '../components/sprite-animation';
import State from '../components/state';
import StateAnimationControl from '../components/state-animation-control';
import { Direction } from '../misc/direction';
import ENTITY_STATE from '../misc/entity-state';

export default class StateAnimationControlSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [State, StateAnimationControl, SpriteAnimation] });
  }

  create(): void {
    this.F.subjectService.subscribe(GAME_EVENTS.ENTITY_DAMAGED, this.tryAnimateHurt.bind(this));
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const control: Component<State> = this.getComponent(entity, State);
      const animsControl: Component<StateAnimationControl> = this.getComponent(entity, StateAnimationControl);
      const anim: Component<SpriteAnimation> = this.getComponent(entity, SpriteAnimation);
      if (animsControl.state.animatedState === undefined) {
        animsControl.state.animatedState = control.state.currentState;
        anim.data.current = this.getAnimation(entity.id, animsControl, control.state.currentState);
      }

      this.updateAnimation(entity.id, anim, animsControl, control.state.currentState);
    }
  }

  private getAnimation(id: string, animsControl:Component<StateAnimationControl>, state: string): string|undefined {
    if (!animsControl.data.animatedStates.includes(state)) {
      return undefined;
    }
    return `${id}|${state}`;
  }

  private updateAnimation(
    id: string,
    anim: Component<SpriteAnimation>,
    animsControl: Component<StateAnimationControl>,
    currentState: string
  ): void {
    if (currentState === animsControl.state.animatedState
      || animsControl.state.animatedState === ENTITY_STATE.HURT_LEFT
      || animsControl.state.animatedState === ENTITY_STATE.HURT_RIGHT) {
      return;
    }
    animsControl.state.animatedState = currentState;
    anim.data.current = this.getAnimation(id, animsControl, currentState);
  }

  private tryAnimateHurt(
    entity: Entity,
    damageValue: number,
    damageDirection: string
  ): void {
    if (!this.impactedEntities.includes(entity)) {
      return;
    }

    const animsControl: Component<StateAnimationControl> = this.getComponent(entity, StateAnimationControl);
    const anim: Component<SpriteAnimation> = this.getComponent(entity, SpriteAnimation);
    const state = Direction.LEFT === damageDirection ? ENTITY_STATE.HURT_LEFT : ENTITY_STATE.HURT_RIGHT;
    if (animsControl.data.animatedStates.includes(state)) {
      const previousState = animsControl.state.animatedState;
      this.updateAnimation(entity.id, anim, animsControl, state);
      setTimeout(() => {
        animsControl.state.animatedState = previousState;
        anim.data.current = this.getAnimation(entity.id, animsControl, previousState);
      }, 500);
    }
  }
}
