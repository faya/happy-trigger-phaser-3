'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import GAME_EVENTS from '../../services/subject/events/game-events';
import Health from '../components/health';

export default class HealthSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [Health] });
  }

  create(): void {
    this.F.subjectService.subscribe(GAME_EVENTS.ENTITY_DAMAGED, this.damageEntity.bind(this));
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const health: Component<Health> = this.getComponent(entity, Health);
      if (health.state.initialized !== true) {
        health.state.current = health.data.max;
        health.state.initialized = true;
      }

      if (health.state.current === 0) {
        this.F.subjectService.dispatch(health.data.eventOnDeath, entity);
      }
    }
  }

  private damageEntity(entity: Entity, damageValue: number): void {
    if (!this.impactedEntities.includes(entity)) {
      throw new Error('Entity does not have a health component');
    }
    const health: Component<Health> = this.getComponent(entity, Health);
    health.state.current = Math.max(health.state.current - damageValue, 0);
  }
}
