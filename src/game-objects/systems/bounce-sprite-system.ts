'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import Facade from '../../services/facade/facade';
import Bouncing from '../components/bouncing';
import Position from '../components/position';
import Sprite from '../components/sprite';

export default class BounceSpriteSystem extends System {
  constructor(
    scene: Phaser.Scene,
    facade: Facade
  ) {
    super(scene, facade);
  }

  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [Position, Sprite, Bouncing] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      this.updateBounce(
        this.getComponent(entity, Position),
        this.getComponent(entity, Sprite),
        this.getComponent(entity, Bouncing));
    }
  }

  private updateBounce(position: Component<Position>, sprite: Component<Sprite>, bounce: Component<Bouncing>): void {
    position.data.yVel = bounce.data.bouncingVelocity *
      (position.data.yVel !== undefined && position.data.yVel > 0 ? 1 : -1);
    position.data.y += position.data.yVel;

    if (position.data.y - 300 > bounce.data.amplitude
      || position.data.y - 300 < -bounce.data.amplitude) {
      position.data.yVel *= -1;
    }

    sprite.state.sprite.y = position.data.y;
  }
}
