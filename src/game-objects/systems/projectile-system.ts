'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import Sprite from '../components/sprite';

export default class ProjectileSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ collectionNames: ['projectile'] });
  }

  // TODO : cleanup projectiles on wave end

  update(): void {
    const outOfBoundsEntityIds: string[] = [];
    for (const entity of this.impactedEntities) {
      const sprite = this.getComponent(entity, Sprite);
      if (sprite.state.sprite === undefined) {
        continue;
      }
      if (this.isOutOfBounds(sprite)) {
        (sprite.state.sprite as Phaser.GameObjects.Sprite).destroy();
        outOfBoundsEntityIds.push(entity.id);
      }
    }
    if (outOfBoundsEntityIds.length !== 0) {
      this.F.entityManager.destroyEntities({ ids: outOfBoundsEntityIds });
    }
  }

  private isOutOfBounds(sprite: Component<Sprite>): boolean {
    return sprite.state.sprite.x < -10 || sprite.state.sprite.x > this.F.config.size.SCALED_GAME_WIDTH + 10
      || sprite.state.sprite.y < -10 || sprite.state.sprite.y > this.F.config.size.SCALED_GAME_HEIGHT + 10;
  }
}
