'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import Position from '../components/position';
import RegularText from '../components/regular-text';

export default class RegularTextSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [RegularText, Position] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const text = this.getComponent(entity, RegularText);
      if (text.state.text === undefined) {
        this.createText(text, this.getComponent(entity, Position));
      }
      this.updateText(text);
    }
  }

  private createText(regularText: Component<RegularText>, position: Component<Position>): void {
    regularText.state.text = this.scene.add.text(
      position.data.x,
      position.data.y,
      regularText.data.text,
      regularText.data.style
    );

    const originToApply = regularText.data.origin !== undefined ? regularText.data.origin : [0.5, 0.5];
    regularText.state.text.setOrigin(originToApply[0], originToApply[1]);

    if (regularText.data.align !== undefined) {
      regularText.state.text.align = regularText.data.align;
    }
  }

  private updateText(regularText: Component<RegularText>): void {
    if (regularText.data.text !== regularText.state.text.text) {
      regularText.state.text.setText(regularText.data.text);
    }
    if (regularText.data.hidden !== undefined) {
      regularText.state.text.setVisible(!regularText.data.hidden);
    }
  }
}
