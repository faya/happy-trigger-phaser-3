'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import ArcadeSprite from '../components/arcade-sprite';
import DamageOverlaping from '../components/damage-overlaping';
import Position from '../components/position';
import Shooting from '../components/shooting';
import Sprite from '../components/sprite';

export default class ShootingSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [Shooting] });
  }

  create(): void {
    this.F.entityManager.createCollection('projectile');
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      this.tryShootProjectile(this.getComponent(entity, Shooting));
    }
  }

  private tryShootProjectile(shooting: Component<Shooting>): void {
    const currentTick = this.scene.time.now;
    if (currentTick < shooting.state.lastProjectileTick + shooting.data.fireRate) {
      return;
    }
    shooting.state.lastProjectileTick = currentTick;
    const projectile: Entity = this.F.entityManager.createEntity('projectile');
    this.F.entityManager.addComponentToEntity(projectile, Position, {
      x: shooting.data.origin[0],
      y: shooting.data.origin[1],
      xVel: Math.cos(Phaser.Math.DegToRad(shooting.data.angle)) * shooting.data.projectileVelocity,
      yVel: Math.sin(Phaser.Math.DegToRad(shooting.data.angle)) * shooting.data.projectileVelocity
    });
    this.F.entityManager.addComponentToEntity(projectile, Sprite, {
      key: shooting.data.projectileSpriteKey,
      frame: shooting.data.projectileSpriteFrame,
      origin: [0.5, 0.5]
    });
    this.F.entityManager.addComponentToEntity(projectile, ArcadeSprite, { disableGravity: true });
    this.F.entityManager.addComponentToEntity(projectile, DamageOverlaping, {
      targetFilters: shooting.data.targetFilters,
      damageValue: shooting.data.damage,
      destroyOnOverlap: true,
      explosionProbability: shooting.data.explosionProbability
    });

    if (shooting.data.onShootEvent !== undefined) {
      this.F.subjectService.dispatch(shooting.data.onShootEvent);
    }
  }
}
