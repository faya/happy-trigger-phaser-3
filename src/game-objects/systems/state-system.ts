'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import GAME_EVENTS from '../../services/subject/events/game-events';
import ArcadeSprite from '../components/arcade-sprite';
import State from '../components/state';
import { Direction } from '../misc/direction';
import ENTITY_STATE from '../misc/entity-state';

export default class StateSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [State] });
  }

  create(): void {
    this.F.subjectService.subscribe(GAME_EVENTS.ENTITY_TOUCHED_PLATFORM, this.onPlatformTouched.bind(this));
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const entityState: Component<State> = this.getComponent(entity, State);
      const body: Phaser.Physics.Arcade.Body =
        this.getComponent(entity, entityState.data.componentType).state[entityState.data.stateComponentKey].body;
      if (entityState.state.isTouchingGround === undefined) {
        entityState.state.isTouchingGround = true;
      }
      this.updateState(entity, entityState, body);
    }
  }

  private onPlatformTouched(
    entityState: Component<State>,
    body: Phaser.Physics.Arcade.Body,
    collided: Phaser.Physics.Arcade.Body): void {
    // if the platform was touched from above, reset jump
    if (body.y < collided.y) {
      entityState.state.jumpCharges = entityState.data.jumpCharges;
      entityState.state.isTouchingGround = true;
    }
  }

  private updateState(
    entity: Entity,
    entityState: Component<State>,
    body: Phaser.Physics.Arcade.Body
  ): void {
    let currentState: string|undefined = undefined;
    let isGravityDisabled: boolean = false;
    try {
      const arcadeSprite = this.getComponent(entity, ArcadeSprite);
      isGravityDisabled = arcadeSprite.data.disableGravity !== undefined ? arcadeSprite.data.disableGravity : false;
    }
    catch (e) {}
    if (isGravityDisabled) {
      // flying
      if (this.getLookingDirection(entityState, body) === Direction.LEFT) {
        currentState = ENTITY_STATE.RUNNING_LEFT;
      }
      else {
        currentState = ENTITY_STATE.RUNNING_RIGHT;
      }
    }
    else if (!entityState.state.isTouchingGround) {
      // jumping
      if (this.getLookingDirection(entityState, body) === Direction.LEFT) {
        currentState = ENTITY_STATE.JUMPING_LEFT;
      }
      else {
        currentState = ENTITY_STATE.JUMPING_RIGHT;
      }
    }
    // on the ground and static
    else if (body.velocity.x === 0) {
      if (this.getLookingDirection(entityState, body) === Direction.LEFT) {
        currentState = ENTITY_STATE.IDLE_LEFT;
      }
      else {
        currentState = ENTITY_STATE.IDLE_RIGHT;
      }
    }
    // on the ground and moving
    else if (this.getLookingDirection(entityState, body) === Direction.LEFT) {
      currentState = ENTITY_STATE.RUNNING_LEFT;
    }
    else {
      currentState = ENTITY_STATE.RUNNING_RIGHT;
    }

    if (currentState !== undefined && currentState !== entityState.state.currentState) {
      entityState.state.currentState = currentState;
    }
  }

  private getLookingDirection(
    entityState: Component<State>,
    body: Phaser.Physics.Arcade.Body
  ): string {
    if (entityState.data.aimDirection !== undefined) {
      return entityState.data.aimDirection;
    }

    return body.velocity.x <= 0 ? Direction.LEFT : Direction.RIGHT;
  }
}
