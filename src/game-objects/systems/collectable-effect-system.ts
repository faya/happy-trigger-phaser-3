'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import CollectableEffect from '../components/collectable-effect';
import HeroSprite from '../components/hero-sprite';
import Sprite from '../components/sprite';
import { EFFECTS } from '../effects';
import BaseEffect from '../effects/effect';

export default class CollectableEffectSystem extends System {
  private managedEffects: BaseEffect[] = [];

  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [CollectableEffect] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const collectable = this.getComponent(entity, CollectableEffect);
      if (collectable.state.registeredOverlaps === undefined) {
        collectable.state.registeredOverlaps = [];
      }

      const sprite = this.getComponent(entity, Sprite).state.sprite;
      if (sprite !== undefined) {
        this.registerOverlapsForCollectersEntities(entity.id, collectable, sprite);
      }
    }

    for (const effect of this.managedEffects) {
      effect.apply();
    }
  }

  private registerOverlapsForCollectersEntities(
    id: string,
    collectable: Component<CollectableEffect>,
    sprite: Phaser.GameObjects.Sprite
  ): void {
    const toRegisterEntities = this.F.entityManager.getEntities(collectable.data.canCollectFilters)
      .filter(e => !collectable.state.registeredOverlaps.includes(e.id));
    collectable.state.registeredOverlaps = [...collectable.state.registeredOverlaps, toRegisterEntities.map(e => e.id)];
    this.scene.physics.add.overlap(sprite, toRegisterEntities.map(e => this.getOverlapableGameObject(e)), (o1, o2) => {
      const collecterEntity = (this.F.entityManager.getEntities(collectable.data.canCollectFilters)
        .find(e => this.getOverlapableGameObject(e) === o2) as Entity);
      const effect = new EFFECTS[collectable.data.effectKey](
        this.scene,
        this.F,
        collecterEntity,
        collectable.data.effectData
      );
      effect.startUp();
      this.managedEffects.push(effect);

      if (collectable.data.destroyOnCollect === true) {
        sprite.destroy();
        this.F.entityManager.destroyEntities({ ids: [id] });
      }
    });
  }

  private getOverlapableGameObject(entity: Entity): Phaser.GameObjects.GameObject {
    try {
      return this.getComponent(entity, Sprite).state.sprite;
    }
    catch (e) {}
    try {
      return this.getComponent(entity, HeroSprite).state.container;
    } catch (e) {}

    throw new Error('Entity holds no physics body to overlap with');
  }
}
