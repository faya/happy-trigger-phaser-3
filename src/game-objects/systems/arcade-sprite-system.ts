'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import ArcadeSprite from '../components/arcade-sprite';
import Position from '../components/position';
import Sprite from '../components/sprite';

export default class ArcadeSpriteSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [ArcadeSprite] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const arcade = this.getComponent(entity, ArcadeSprite);
      if (arcade.state.initialized !== true) {
        this.enablePhysics(
          this.getComponent(entity, Sprite),
          arcade,
          this.getComponent(entity, Position)
        );
      }
    }
  }

  private enablePhysics(
    sprite: Component<Sprite>,
    arcade: Component<ArcadeSprite>,
    position: Component<Position>
  ): void {
    const bodyType = arcade.data.immovable !== undefined && arcade.data.immovable ? 1 : 0;
    const width = bodyType === 1 ? sprite.state.sprite.displayWidth : sprite.state.sprite.width;
    const height = bodyType === 1 ? sprite.state.sprite.displayHeight : sprite.state.sprite.height;
    this.scene.physics.world.enable(sprite.state.sprite, bodyType);
    sprite.state.sprite.body.setSize(width, height);
    if (position.data.xVel !== undefined) {
      sprite.state.sprite.body.setVelocityX(position.data.xVel * this.F.config.size.SCALE);
    }
    if (position.data.yVel !== undefined) {
      sprite.state.sprite.body.setVelocityY(position.data.yVel * this.F.config.size.SCALE);
    }
    if (arcade.data.collidesWorldBounds !== undefined) {
      sprite.state.sprite.body.setCollideWorldBounds(arcade.data.collidesWorldBounds);
    }
    if (arcade.data.disableGravity !== undefined) {
      sprite.state.sprite.body.setAllowGravity(!arcade.data.disableGravity);
    }
    arcade.state.initialized = true;
  }
}
