'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import { EntityFilter } from '../../services/ecs/entities/entity-manager';
import System from '../../services/ecs/systems/system';
import GAME_EVENTS from '../../services/subject/events/game-events';
import ArcadeSprite from '../components/arcade-sprite';
import DamageOverlaping from '../components/damage-overlaping';
import Explosion from '../components/explosion';
import Health from '../components/health';
import HeroSprite from '../components/hero-sprite';
import Position from '../components/position';
import Sprite from '../components/sprite';
import { Direction } from '../misc/direction';

const DAMAGE_REPEAT_DELAY: number = 250;

export default class DamageOverlapingSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [DamageOverlaping, Sprite, ArcadeSprite] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const collider: Component<DamageOverlaping> = this.getComponent(entity, DamageOverlaping);
      if (collider.state.lastCollisionTicks === undefined) {
        collider.state.lastCollisionTicks = {};
        collider.state.registeredOverlaps = [];
      }
      const sprite = this.getComponent(entity, Sprite).state.sprite;
      if (sprite !== undefined) {
        this.registerOverlapsForDamageableEntities(entity.id, collider, sprite);
      }
    }
  }

  private registerOverlapsForDamageableEntities(
    id: string,
    collider: Component<DamageOverlaping>,
    sprite: Phaser.GameObjects.Sprite
  ): void {
    const toRegisterEntities = this.getDamageableColliders(collider.data.targetFilters)
      .filter(e => !collider.state.registeredOverlaps.includes(e.id));
    collider.state.registeredOverlaps = [...collider.state.registeredOverlaps, ...toRegisterEntities.map(e => e.id)];
    this.scene.physics.add.overlap(sprite, toRegisterEntities.map(e => this.getDamageableGameObject(e)), (o1, o2) => {
      const overlapedEntity = (toRegisterEntities.find(e => this.getDamageableGameObject(e) === o2) as Entity);
      const currentTick = this.scene.time.now;
      if (currentTick < collider.state.lastCollisionTicks[overlapedEntity.id] + DAMAGE_REPEAT_DELAY) {
        return;
      }
      collider.state.lastCollisionTicks[overlapedEntity.id] = currentTick;

      try {
        const health = this.getComponent(overlapedEntity, Health);
        if (health.state.current <= 0) {
          // already dead, do not damage
          return;
        }
      }
      catch (e) {}

      const collisionDirection = (sprite.body as Phaser.Physics.Arcade.Body).center.x >
        (o2.body as Phaser.Physics.Arcade.Body).center.x ? Direction.LEFT : Direction.RIGHT;
      this.F.subjectService
        .dispatch(GAME_EVENTS.ENTITY_DAMAGED, overlapedEntity, collider.data.damageValue, collisionDirection);

      if (collider.data.explosionProbability !== undefined
        && Math.random() <= collider.data.explosionProbability) {
        // collision provoked an explosion
        const explosion = this.F.entityManager.createEntity('explosion');
        this.F.entityManager.addComponentToEntity(explosion, Position, {
          x: (sprite.body as Phaser.Physics.Arcade.Body).center.x,
          y: (sprite.body as Phaser.Physics.Arcade.Body).center.y
        });
        this.F.entityManager.addComponentToEntity(explosion, Explosion, {
          radius: 100
        });
      }

      if (collider.data.destroyOnOverlap) {
        // remove element causing damage
        sprite.destroy();
        this.F.entityManager.destroyEntities({ ids: [id] });
      }
    });
  }

  protected getDamageableColliders(filters: EntityFilter): Entity[] {
    return this.F.entityManager.getEntities(filters);
  }

  private getDamageableGameObject(entity: Entity): Phaser.GameObjects.GameObject {
    try {
      return this.getComponent(entity, Sprite).state.sprite;
    }
    catch (e) {}
    try {
      return this.getComponent(entity, HeroSprite).state.container;
    } catch (e) {}

    throw new Error('Entity holds no physics body to collide with');
  }
}
