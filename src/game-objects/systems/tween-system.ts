'use strict';

import Component from '../../services/ecs/components/component';
import Entity from '../../services/ecs/entities/entity';
import System from '../../services/ecs/systems/system';
import TweenQueue, { Tween } from '../components/tween';

export default class TweenSystem extends System {
  protected get impactedEntities(): Entity[] {
    return this.F.entityManager.getEntities({ components: [TweenQueue] });
  }

  update(): void {
    for (const entity of this.impactedEntities) {
      const queue = this.getComponent(entity, TweenQueue);
      if (queue.state.playing !== true && queue.data.items.length > 0) {
        this.cleanupPreviousTween(queue);
        const nextTween = queue.data.items.splice(0, 1);
        this.playTween(entity, queue, nextTween[0]);
      }
    }
  }

  private cleanupPreviousTween(queue: Component<TweenQueue>): void {
    if (queue.state.current !== undefined) {
      (queue.state.current as Phaser.Tweens.Tween).stop();
      queue.state.current = undefined;
    }
  }

  private playTween(entity: Entity, queue: Component<TweenQueue>, tween: Tween): void {
    const targets = this.getTarget(entity, tween);

    const options = Object.assign({
      targets,
      duration: tween.duration,
      ease: tween.ease,
      delay: tween.delay,
      yoyo: tween.yoyo,
      loop: tween.loop !== undefined ? -1 : 0 // -1 <=> infinite number of loops
    }, tween.properties);
    queue.state.current = this.scene.tweens.add(options);
    queue.state.playing = true;
    (queue.state.current as Phaser.Tweens.Tween).setCallback('onComplete', (q: Component<TweenQueue>) => {
      q.state.playing = false;
    }, [queue]);
  }

  private getTarget(entity: Entity, tween: Tween): any {
    const component = this.getComponent(entity, tween.componentType);
    if (!Object.keys(component.state).includes(tween.stateTweenTargetKey)) {
      throw new Error(`Component state holds no property ${tween.stateTweenTargetKey}`);
    }
    return component.state[tween.stateTweenTargetKey];
  }
}
