'use strict';

import { BIRD_BLUE } from './data/bird-blue';
import { BIRD_GREEN } from './data/bird-green';
import { BLOB_GRAY_SPIKY } from './data/blob-gray-spiky';
import { BLOB_GREEN } from './data/blob-green';
import { BLOB_GREEN_SPIKY } from './data/blob-green-spiky';
import { BLOB_ORANGE } from './data/blob-orange';
import { BLOB_PINK } from './data/blob-pink';
import { CYCLOP_BLUE } from './data/cyclop-blue';
import { CYCLOP_ORANGE } from './data/cyclop-orange';
import { FLY_GRAY } from './data/fly-gray';
import { FLY_GREEN } from './data/fly-green';
import { FLY_RED } from './data/fly-red';
import { HORN_BLUE } from './data/horn-blue';
import { HORN_GREEN } from './data/horn-green';
import { HORNET_RED } from './data/hornet-red';
import { PEA_GREEN } from './data/pea-green';
import { ROCK_SPIKY } from './data/rock-spicky';
import { ZOMBIE_GREEN } from './data/zombie-green';
import { ZOMBIE_RED } from './data/zombie-red';

export interface EnnemyData {
  assetKey: string;
  health: number;
  movementBehavior: BehaviorData;
  attackBehavior: BehaviorData;
  spriteWidth: number;
  spriteHeight: number;
}

export interface BehaviorData {
  key: string;
  options: any;
}

export const ENNEMY_KEYS = {
  BLOB_PINK: 'ennemy:blob-pink',
  BLOB_ORANGE: 'ennemy:blob-orange',
  BLOB_GREEN: 'ennemy:blob-green',
  HORN_BLUE: 'ennemy:horn-blue',
  HORN_GREEN: 'ennemy:horn-green',
  CYCLOP_BLUE: 'ennemy:cyclop-blue',
  CYCLOP_ORANGE: 'ennemy:cyclop-orange',
  PEA_GREEN: 'ennemy:pea-green',
  ZOMBIE_GREEN: 'ennemy:zombie-green',
  ZOMBIE_RED: 'ennemy:zombie-red',
  ROCK_SPIKY: 'ennemy:rock-spiky',
  BLOB_GREEN_SPIKY: 'ennemy:blob-green-spiky',
  BLOB_GRAY_SPIKY: 'ennemy:blob-gray-spiky',
  FLY_GRAY: 'ennemy:fly-gray',
  FLY_GREEN: 'ennemy:fly-green',
  BIRD_BLUE: 'ennemy:bird-blue',
  BIRD_GREEN: 'ennemy:bird-green',
  FLY_RED: 'ennemy:fly-red',
  HORNET_RED: 'ennemy:hornet-red'
};

const ENNEMY_DATA: { [key: string]: EnnemyData } = {
  [ENNEMY_KEYS.BLOB_PINK]: BLOB_PINK,
  [ENNEMY_KEYS.BLOB_ORANGE]: BLOB_ORANGE,
  [ENNEMY_KEYS.BLOB_GREEN]: BLOB_GREEN,
  [ENNEMY_KEYS.HORN_BLUE]: HORN_BLUE,
  [ENNEMY_KEYS.HORN_GREEN]: HORN_GREEN,
  [ENNEMY_KEYS.CYCLOP_BLUE]: CYCLOP_BLUE,
  [ENNEMY_KEYS.CYCLOP_ORANGE]: CYCLOP_ORANGE,
  [ENNEMY_KEYS.PEA_GREEN]: PEA_GREEN,
  [ENNEMY_KEYS.ZOMBIE_GREEN]: ZOMBIE_GREEN,
  [ENNEMY_KEYS.ZOMBIE_RED]: ZOMBIE_RED,
  [ENNEMY_KEYS.ROCK_SPIKY]: ROCK_SPIKY,
  [ENNEMY_KEYS.BLOB_GREEN_SPIKY]: BLOB_GREEN_SPIKY,
  [ENNEMY_KEYS.BLOB_GRAY_SPIKY]: BLOB_GRAY_SPIKY,
  [ENNEMY_KEYS.FLY_GRAY]: FLY_GRAY,
  [ENNEMY_KEYS.FLY_GREEN]: FLY_GREEN,
  [ENNEMY_KEYS.BIRD_BLUE]: BIRD_BLUE,
  [ENNEMY_KEYS.BIRD_GREEN]: BIRD_GREEN,
  [ENNEMY_KEYS.FLY_RED]: FLY_RED,
  [ENNEMY_KEYS.HORNET_RED]: HORNET_RED
};

export default ENNEMY_DATA;
