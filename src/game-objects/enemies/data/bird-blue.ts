'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const BIRD_BLUE = {
  assetKey: 'bird-blue',
  health: 3,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.FLY_PATROL,
    options: { velocity: 400, waveAmplitude: 150, waveSpeed: 800 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.DO_NOTHING, options: {} },
  spriteWidth: 175,
  spriteHeight: 120
};
