'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';
import { Direction } from '../../misc/direction';

export const FLY_RED = {
  assetKey: 'fly-red',
  health: 3,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.FLY_TOWARDS_HERO_HORIZONTAL,
    options: { velocity: 300, waveAmplitude: 100, waveSpeed: 800 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.FIRE_PROJECTILE_DIRECTION,
    options: { direction: Direction.DOWN, fireRate: 2000, damage: 1, velocity: 600 } },
  spriteWidth: 135,
  spriteHeight: 100
};
