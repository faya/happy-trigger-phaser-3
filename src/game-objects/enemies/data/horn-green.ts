'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const HORN_GREEN = {
  assetKey: 'horn-green',
  health: 1,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.PATROL_BLIND, options: { velocity: 1000 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.DO_NOTHING, options: {} },
  spriteWidth: 110,
  spriteHeight: 100
};
