'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const ROCK_SPIKY = {
  assetKey: 'rock-spiky',
  health: 10,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.STAND_STILL, options: {} },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.DO_NOTHING, options: {} },
  spriteWidth: 180,
  spriteHeight: 180
};
