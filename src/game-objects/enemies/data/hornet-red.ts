'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const HORNET_RED = {
  assetKey: 'hornet-red',
  health: 3,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.FLY_TOWARDS_HERO_HORIZONTAL,
    options: { velocity: 300, waveAmplitude: 200, waveSpeed: 800 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.EXPLODE_PROXIMITY,
    options: { damage: 2, range: 100 } },
  spriteWidth: 120,
  spriteHeight: 100
};
