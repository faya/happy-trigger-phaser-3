'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const ZOMBIE_RED = {
  assetKey: 'zombie-red',
  health: 1,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.PATROL_TOWARDS_HERO,
    options: { velocity: 600 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.EXPLODE_TIMER,
    options: { damage: 2, timer: 3000 } },
  spriteWidth: 90,
  spriteHeight: 100
};
