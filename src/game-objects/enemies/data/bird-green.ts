'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';
import { Direction } from '../../misc/direction';

export const BIRD_GREEN = {
  assetKey: 'bird-green',
  health: 1,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.FLY_PATROL,
    options: { velocity: 300, waveAmplitude: 100, waveSpeed: 800 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.FIRE_PROJECTILE_DIRECTION,
    options: { direction: Direction.DOWN, fireRate: 2000, damage: 1, velocity: 600 } },
  spriteWidth: 145,
  spriteHeight: 120
};
