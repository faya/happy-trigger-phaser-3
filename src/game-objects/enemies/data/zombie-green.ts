'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const ZOMBIE_GREEN = {
  assetKey: 'zombie-green',
  health: 3,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.PATROL_TOWARDS_HERO,
    options: { velocity: 300 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.EXPLODE_PROXIMITY,
    options: { damage: 2, range: 100 } },
  spriteWidth: 88,
  spriteHeight: 100
};
