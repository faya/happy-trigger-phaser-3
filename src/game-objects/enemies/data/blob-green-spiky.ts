'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const BLOB_GREEN_SPIKY = {
  assetKey: 'blob-green-spiky',
  health: 6,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.STAND_STILL, options: {} },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.EXPLODE_PROXIMITY,
    options: { damage: 4, range: 100 } },
  spriteWidth: 140,
  spriteHeight: 140
};
