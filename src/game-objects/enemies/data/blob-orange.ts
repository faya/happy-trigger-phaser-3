'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const BLOB_ORANGE = {
  assetKey: 'blob-orange',
  health: 3,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.PATROL_BLIND, options: { velocity: 300 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.FIRE_PROJECTILE_HERO,
    options: { fireRate: 2000, damage: 1, velocity: 600 } },
  spriteWidth: 135,
  spriteHeight: 100
};
