'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const FLY_GREEN = {
  assetKey: 'fly-green',
  health: 3,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.FLY_STILL,
    options: { waveAmplitude: 100, waveSpeed: 800 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.FIRE_PROJECTILE_HERO,
    options: { fireRate: 2000, damage: 1, velocity: 600 } },
  spriteWidth: 120,
  spriteHeight: 100
};
