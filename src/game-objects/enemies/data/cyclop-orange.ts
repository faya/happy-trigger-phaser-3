'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const CYCLOP_ORANGE = {
  assetKey: 'cyclop-orange',
  health: 3,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.PATROL_JUMP,
    options: { velocity: 200, jumpDelay: 1000, jumpVelocity: 1000 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.FIRE_PROJECTILE_HERO,
    options: { fireRate: 2000, damage: 1, velocity: 600 } },
  spriteWidth: 90,
  spriteHeight: 100
};
