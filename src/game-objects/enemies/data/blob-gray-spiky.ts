'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const BLOB_GRAY_SPIKY = {
  assetKey: 'blob-gray-spiky',
  health: 6,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.STAND_STILL, options: {} },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.FIRE_PROJECTILE_HERO,
    options: { fireRate: 2000, damage: 1, velocity: 600 } },
  spriteWidth: 180,
  spriteHeight: 140
};
