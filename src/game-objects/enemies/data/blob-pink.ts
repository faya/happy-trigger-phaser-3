'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const BLOB_PINK = {
  assetKey: 'blob-pink',
  health: 1,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.PATROL_BLIND, options: { velocity: 400 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.DO_NOTHING, options: {} },
  spriteWidth: 118,
  spriteHeight: 100
};
