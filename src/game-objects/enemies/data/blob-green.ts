'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const BLOB_GREEN = {
  assetKey: 'blob-green',
  health: 1,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.PATROL_BLIND, options: { velocity: 800 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.DO_NOTHING, options: {} },
  spriteWidth: 150,
  spriteHeight: 120
};
