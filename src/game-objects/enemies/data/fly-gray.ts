'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const FLY_GRAY = {
  assetKey: 'fly-gray',
  health: 8,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.FLY_STILL,
    options: { waveAmplitude: 200, waveSpeed: 800 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.DO_NOTHING, options: {} },
  spriteWidth: 125,
  spriteHeight: 100
};
