'use strict';

import { ATTACK_BEHAVIORS_KEYS } from '../../behaviors/attack';
import { MOVEMENT_BEHAVIORS_KEYS } from '../../behaviors/movement';

export const CYCLOP_BLUE = {
  assetKey: 'cyclop-blue',
  health: 1,
  movementBehavior: { key: MOVEMENT_BEHAVIORS_KEYS.PATROL_JUMP,
    options: { velocity: 300, jumpDelay: 1000, jumpVelocity: 1000 } },
  attackBehavior: { key: ATTACK_BEHAVIORS_KEYS.DO_NOTHING, options: {} },
  spriteWidth: 85,
  spriteHeight: 100
};
