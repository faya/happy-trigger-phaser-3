import Facade from '../services/facade/facade';

'use strict';

export default abstract class GameObject {
  constructor(
    protected scene: Phaser.Scene,
    private facade: Facade
  ) {}

  protected get F(): Facade {
    return this.facade;
  }

  preload() {}
  create() {}
  update(time?: number, delta?: number) {}
  render() {}
}
