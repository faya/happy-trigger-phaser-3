'use strict';

import Entity from '../../services/ecs/entities/entity';
import SystemRoutine from '../../services/ecs/systems/system-routine';
import Facade from '../../services/facade/facade';

export default abstract class BaseEffect extends SystemRoutine {
  constructor(
    protected scene: Phaser.Scene,
    protected F: Facade,
    public entity: Entity,
    protected options: any) {
    super(scene, F, options);
  }
}
