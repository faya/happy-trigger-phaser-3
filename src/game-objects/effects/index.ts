'use strict';

import HealthBonusEffect from './bonus/health-bonus-effect';
import ScoreBonusEffect from './bonus/score-bonus-effect';

export const EFFECT_KEYS = {
  HEALTH_BONUS: 'EFFECT:HEALTH_BONUS',
  SCORE_BONUS: 'EFFECT:SCORE_BONUS'
};

export const EFFECTS = {
  [EFFECT_KEYS.HEALTH_BONUS]: HealthBonusEffect,
  [EFFECT_KEYS.SCORE_BONUS]: ScoreBonusEffect
};
