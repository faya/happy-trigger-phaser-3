'use strict';

import REGISTRY from '../../../registry';
import HERO_EVENTS from '../../../services/subject/events/hero-events';
import BaseEffect from '../effect';

export default class ScoreBonusEffect extends BaseEffect {
  startUp(): void {
    try {
      this.scene.registry.set(REGISTRY.SCORE, this.scene.registry.get(REGISTRY.SCORE) + this.options.bonusValue);
      this.F.subjectService.dispatch(HERO_EVENTS.BONUS_SCORE);
    }
    catch (e) {}
  }
}
