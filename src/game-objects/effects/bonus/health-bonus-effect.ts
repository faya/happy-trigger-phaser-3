'use strict';

import Component from '../../../services/ecs/components/component';
import HERO_EVENTS from '../../../services/subject/events/hero-events';
import Health from '../../components/health';
import BaseEffect from '../effect';

export default class HealthBonusEffect extends BaseEffect {
  startUp(): void {
    try {
      const healthComponent: Component<Health> = this.F.entityManager.getComponentFromEntity(this.entity, Health);
      healthComponent.state.current = Math.min(
        healthComponent.data.max,
        healthComponent.state.current + this.options.bonusValue
      );
      this.F.subjectService.dispatch(HERO_EVENTS.BONUS_HEALTH);
    }
    catch (e) {}
  }
}
