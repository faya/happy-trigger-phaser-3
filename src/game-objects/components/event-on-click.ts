'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default class EventOnClick implements IComponentData {
  componentType: {new(): IComponentData};
  stateObjectKey: string;
  event: string;
}
