'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default abstract class BaseControllable implements IComponentData {
  componentType: {new(): IComponentData};
  stateBodyKey: string;
  horizontalVelocity: number;
  jumpVelocity: number;
}
