'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default class Explosion implements IComponentData {
  radius: number;
  damage?: number;
  color?: number;
}
