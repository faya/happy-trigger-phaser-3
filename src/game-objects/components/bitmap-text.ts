'use strict';

import BaseText from './text';

export default class BitmapText extends BaseText {
  font: string;
  size: number;
}
