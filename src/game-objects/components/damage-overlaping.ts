'use strict';

import { IComponentData } from '../../services/ecs/components/component';
import { EntityFilter } from '../../services/ecs/entities/entity-manager';

export default class DamageOverlaping implements IComponentData {
  targetFilters: EntityFilter;
  damageValue: number;
  destroyOnOverlap: boolean;
  explosionProbability?: number;
}
