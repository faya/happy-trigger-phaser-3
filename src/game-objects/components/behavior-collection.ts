'use strict';

import { IComponentData } from '../../services/ecs/components/component';
import { BehaviorData } from '../enemies/ennemy-data';

export default class BehaviorCollection implements IComponentData {
  items: [string, BehaviorData, any][];
}
