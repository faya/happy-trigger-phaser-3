'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default class Sprite implements IComponentData {
  key: string;
  origin?: [number, number];
  frame?: number;
  angle?: number;
  hidden?: boolean;
  alpha?: number;
  scale?: number;
  animations?: [string, GenerateFrameNumbersConfig, number, boolean][];
}
