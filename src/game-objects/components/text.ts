'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default abstract class BaseText implements IComponentData {
  text: string;
  align?: number;
  origin?: [number, number];
  hidden?: boolean;
}
