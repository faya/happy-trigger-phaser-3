'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default class SpriteAnimation implements IComponentData {
  componentType: {new(): IComponentData};
  stateSpriteKey: string;
  current?: string;
  repeat?: boolean;
  repeatDelay?: number;
}
