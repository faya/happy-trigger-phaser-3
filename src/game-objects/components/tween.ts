'use strict';

import { IComponentData } from '../../services/ecs/components/component';
import Sprite from './sprite';

export default class TweenQueue implements IComponentData {
  items: Tween[] = [];
}

export class Tween implements IComponentData {
  componentType: {new(): IComponentData};
  stateTweenTargetKey: string;
  properties: any;
  duration: number;
  ease: string;
  delay?: number;
  yoyo?: boolean;
  loop?: boolean;
  permanent: boolean;
}
