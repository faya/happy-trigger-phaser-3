'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default class State implements IComponentData {
  componentType: {new(): IComponentData};
  stateComponentKey: string;
  jumpCharges: number;
  aimDirection?: string;
}
