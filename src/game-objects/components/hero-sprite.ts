'use strict';

import { IComponentData } from '../../services/ecs/components/component';
import Sprite from './sprite';

export default class HeroSprite implements IComponentData {
  spritesData: { [key: string]: Sprite; };
  spritesPos: { [key: string]: [number, number] };
}
