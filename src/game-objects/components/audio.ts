'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export abstract class Audio implements IComponentData {
  key: string;
  volume: number;
}

export class BackgroundAudio extends Audio {}

export class OnEventAudio extends Audio {
  events: string[];
}

export class OnEventMultipleAudio implements IComponentData {
  items: OnEventAudio[];
}
