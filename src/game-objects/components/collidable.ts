'use strict';

import { IComponentData } from '../../services/ecs/components/component';
import { EntityFilter } from '../../services/ecs/entities/entity-manager';

export default class Collidable implements IComponentData {
  componentType: {new(): IComponentData};
  stateCollidableKey: string;
  collidesWith: ICollideWithData[];
}

export interface ICollideWithData {
  filters: EntityFilter;
  componentType: {new(): IComponentData};
  stateCollidableKey: string;
  collisionEvent?: string;
}
