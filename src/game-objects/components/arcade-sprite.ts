'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default class ArcadeSprite implements IComponentData {
  collidesWorldBounds?: boolean;
  immovable?: boolean;
  disableGravity?: boolean;
}
