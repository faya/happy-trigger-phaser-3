'use strict';

import BaseControllable from './controllable';

export default class ControllableByMatrix extends BaseControllable {
  stop?: boolean = false;
  moveLeft?: boolean = false;
  moveRight?: boolean = false;
  jump?: boolean = false;
}
