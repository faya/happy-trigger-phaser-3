'use strict';

import { IComponentData } from '../../services/ecs/components/component';
import { EntityFilter } from '../../services/ecs/entities/entity-manager';

export default class CollectableEffect implements IComponentData {
  effectKey: string;
  effectData: any;
  canCollectFilters: EntityFilter;
  destroyOnCollect?: boolean;
}
