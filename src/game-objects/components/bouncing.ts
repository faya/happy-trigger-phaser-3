'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default class Bouncing implements IComponentData {
  bouncingVelocity: number;
  amplitude: number;
}
