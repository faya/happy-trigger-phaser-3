'use strict';

import { IComponentData } from '../../services/ecs/components/component';
import { SpawnData } from '../waves/wave-interfaces';

export default class Spawn implements IComponentData {
  config: SpawnData;
}
