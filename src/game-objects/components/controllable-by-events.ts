'use strict';

import BaseControllable from './controllable';

export default class ControllableByEvents extends BaseControllable {
  leftEvent: string;
  rightEvent: string;
  jumpEvent: string;
}
