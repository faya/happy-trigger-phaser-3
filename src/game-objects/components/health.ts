'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default class Health implements IComponentData {
  max: number;
  eventOnDeath: string;
}
