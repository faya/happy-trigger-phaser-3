'use strict';

import { IComponentData } from '../../services/ecs/components/component';
import { EntityFilter } from '../../services/ecs/entities/entity-manager';

export default class Shooting implements IComponentData {
  targetFilters: EntityFilter;
  origin: [number, number];
  damage: number;
  angle: number;
  fireRate: number;
  projectileSpriteKey: string;
  projectileSpriteFrame: number;
  projectileVelocity: number;
  onShootEvent?: string;
  explosionProbability?: number;
}
