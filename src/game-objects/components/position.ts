'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default class Position implements IComponentData {
  x: number;
  y: number;
  z?: number;
  xVel?: number;
  yVel?: number;
  zVel?: number;
}
