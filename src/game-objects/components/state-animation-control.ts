'use strict';

import { IComponentData } from '../../services/ecs/components/component';

export default class StateAnimationControl implements IComponentData {
  animatedStates: string[];
}
