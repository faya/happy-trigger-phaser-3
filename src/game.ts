'use strict';

import * as Phaser from 'phaser';
import Facade from './services/facade/facade';

export const PHASER_GAME_CONFIG: GameConfig = {
  title: 'Happy Trigger',
  width: undefined,
  height: undefined,
  type: Phaser.AUTO,
  parent: 'game',
  backgroundColor: 0x000000,
  physics: undefined
};

export default class Game extends Phaser.Game {
  constructor(config: GameConfig, private facade: Facade) {
    super(config);
    this.performResize(this.facade.config.size.SCALED_GAME_WIDTH, this.facade.config.size.SCALED_GAME_HEIGHT);
    this.facade.config.game.IS_MOBILE = !this.device.os.desktop;
  }

  private performResize(width: number, height: number): void {
    this.resize(width, height);
    this.renderer.resize(width, height);
    this.scene.resize(width, height);
  }
}
