'use strict';

export interface IRegistryKeys {
  SCORE: string;
  CURRENT_WAVE: string;
  NB_REMAINING_ENNEMIES: string;
  AIM_ANGLE: string;
}

const REGISTRY: IRegistryKeys = {
  SCORE: 'REGISTRY:SCORE',
  CURRENT_WAVE: 'REGISTRY:CURRENT_WAVE',
  NB_REMAINING_ENNEMIES: 'REGISTRY:NB_REMAINING_ENNEMIES',
  AIM_ANGLE: 'REGISTRY:AIM_ANGLE'
};

export default REGISTRY;
