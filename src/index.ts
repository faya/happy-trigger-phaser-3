'use strict';

/// <reference path="./phaser.d.ts"/>

import IGlobalConfig, { CONFIG } from './config';
import Game, { PHASER_GAME_CONFIG } from './game';
import SpriteSystem from './game-objects/systems/sprite-system';
import SceneManager from './scene-manager';
import EntityManager from './services/ecs/entities/entity-manager';
import Facade from './services/facade/facade';
import { UtilsService } from './services/helper/utils-service';
import SubjectService from './services/subject/subject-service';

(() => {
  window.onload = () => {
    const dependencies = instantiateDependencies();
    const facade: Facade = new Facade(
      dependencies.entityManager,
      dependencies.subjectService,
      dependencies.config
    );

    const phaserConfig: GameConfig = getTunedPhaserGameConfig(facade.config);
    const game: Phaser.Game = new Game(phaserConfig, facade);
    const sceneManager: SceneManager = new SceneManager(game, facade);
    sceneManager.start();
  };

  function instantiateDependencies(): any {
    const config: IGlobalConfig = CONFIG;
    SpriteSystem.setupScale(config);

    return {
      config,
      entityManager: new EntityManager(new UtilsService()),
      subjectService: new SubjectService()
    };
  }

  function getTunedPhaserGameConfig(config: IGlobalConfig): GameConfig {
    const output: GameConfig = JSON.parse(JSON.stringify(PHASER_GAME_CONFIG));
    output.width = config.size.GAME_WIDTH;
    output.height = config.size.GAME_HEIGHT;
    output.physics = {
      default: 'arcade',
      arcade: {
        gravity: { y: config.game.GRAVITY * config.size.SCALE },
        debug: false
      }
    };
    return output;
  }
})();
