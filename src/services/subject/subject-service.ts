'use strict';

export default class SubjectService {
  protected topics: {[key: string]: Listener[] } = {};

  subscribe(event: string, callback: (...args: any[]) => void, priority: number = 50): void {
    this.createSignalIfNew(event);

    const listener: Listener = new Listener(callback, priority);
    this.topics[event].push(listener);
  }

  clearSubscribers(event: string): void {
    this.topics[event] = [];
  }

  clearSubscribersForAllEvents(): void {
    for (const event of Object.keys(this.topics)) {
      this.topics[event] = [];
    }
  }

  dispatch(event: string, ...payload: any[]): void {
    this.createSignalIfNew(event);

    this.topics[event]
      .sort((a, b) => b.priority - a.priority)
      .forEach(listener => listener.callback(...payload));
  }

  delayedDispatch(event: string, timeout: number = 500, ...payload: any[]): void {
    setTimeout(() => this.dispatch(event, ...payload), timeout);
  }

  private createSignalIfNew(event: string): void {
    if (!Object.keys(this.topics).includes(event)) {
      this.topics[event] = [];
    }
  }
}

export const EVENT_PRIORITIES = {
  HIGH: 100,
  MEDIUM: 50,
  LOW: 0
};

export class Listener {
  constructor(public callback: (...args: any[]) => void, public priority: number = 0) {}
}
