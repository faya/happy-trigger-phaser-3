'use strict';

import IBaseEvents from './base-events';

interface InputEvents extends IBaseEvents {
  KEY_PRESSED: string;
}

const INPUT_EVENTS : InputEvents = {
  KEY_PRESSED: 'INPUT:KEY_PRESSED'
};

export default INPUT_EVENTS;
