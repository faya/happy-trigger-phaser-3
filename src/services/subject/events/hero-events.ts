'use strict';

import IBaseEvents from './base-events';

export interface IHeroEvents extends IBaseEvents {
  MOVE_LEFT: string;
  MOVE_RIGHT: string;
  JUMP: string;
  SHOOT: string;
  DAMAGED: string;
  DIED: string;
  BONUS_HEALTH: string;
  BONUS_SCORE: string;
}

const HERO_EVENTS: IHeroEvents = {
  MOVE_LEFT: 'HERO:MOVE_LEFT',
  MOVE_RIGHT: 'HERO:MOVE_RIGHT',
  JUMP: 'HERO:JUMP',
  SHOOT: 'HERO:SHOOT',
  DAMAGED: 'HERO:DAMAGED',
  DIED: 'HERO:DIED',
  BONUS_HEALTH: 'HERO:BONUS_HEALTH',
  BONUS_SCORE: 'HERO:BONUS_SCORE'
};

export default HERO_EVENTS;
