'use strict';

import IBaseEvents from './base-events';

export interface ISceneEvents extends IBaseEvents {
  START_LOADING: string;
  START_TITLE: string;
  START_GAME: string;
  START_GAME_OVER: string;
  START_CREDITS: string;
  SCENE_CHANGED: string;
}

const SCENE_EVENTS: ISceneEvents = {
  START_LOADING: 'SCENE:START_LOADING',
  START_TITLE: 'SCENE:START_TITLE',
  START_GAME: 'SCENE:START_GAME',
  START_GAME_OVER: 'SCENE:START_GAME_OVER',
  START_CREDITS: 'SCENE:START_CREDITS',
  SCENE_CHANGED: 'SCENE:SCENE_CHANGED'
};

export default SCENE_EVENTS;
