'use strict';

import IBaseEvents from './base-events';

export interface IWaveEvents extends IBaseEvents {
  ENNEMY_DIED: string;
  WAVE_START: string;
  WAVE_END: string;
  LAST_WAVE_END: string;
}

const WAVE_EVENTS: IWaveEvents = {
  ENNEMY_DIED: 'WAVE:ENNEMY_DIED',
  WAVE_START: 'WAVE:WAVE_START',
  WAVE_END: 'WAVE:WAVE_END',
  LAST_WAVE_END: 'WAVE:LAST_WAVE_END'
};

export default WAVE_EVENTS;
