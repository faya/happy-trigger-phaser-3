'use strict';

import IBaseEvents from './base-events';

export interface IGameEvents extends IBaseEvents {
  RESIZE: string;
  PAUSED: string;
  RESUMED: string;
  ENTITY_TOUCHED_PLATFORM: string;
  ENTITY_DAMAGED: string;
  EXPLOSION: string;
}

const GAME_EVENTS: IGameEvents = {
  RESIZE: 'GAME:RESIZE',
  PAUSED: 'GAME:PAUSED',
  RESUMED: 'GAME:RESUMED',
  ENTITY_TOUCHED_PLATFORM: 'GAME:ENTITY_TOUCHED_PLATFORM',
  ENTITY_DAMAGED: 'GAME:ENTITY_DAMAGED',
  EXPLOSION: 'GAME:EXPLOSION',
};

export default GAME_EVENTS;
