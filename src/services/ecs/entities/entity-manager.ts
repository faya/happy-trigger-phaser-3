'use strict';

import { IUtilsService } from '../../helper/utils-service';
import Component, { ComponentFactory, IComponentData } from '../components/component';
import Entity from './entity';

export default class EntityManager {
  private entities: Entity[] = [];
  private componentMatrix: Map<{new(): IComponentData}, { [key: string]: boolean; }>
    = new Map<{new(): IComponentData}, { [key: string]: boolean; }>();
  private tagMatrix: { [key: string]: { [key: string]: boolean; }; } = {};
  private collectionMatrix: { [key: string]: { [key: string]: boolean; }; } = {};

  constructor(
    private utilsService: IUtilsService
  ) {}

  createEntity(collectionName: string, tags: string[] = []): Entity {
    const entity = new Entity();
    entity.id = this.utilsService.newGuid();
    entity.components = [];

    if (!Object.keys(this.collectionMatrix).includes(collectionName)) {
      throw new Error(`Invalid collection name ${collectionName}`);
    }
    this.collectionMatrix[collectionName][entity.id] = true;

    for (const tag of tags) {
      if (this.tagMatrix[tag] === undefined) {
        this.tagMatrix[tag] = {};
      }
      this.tagMatrix[tag][entity.id] = true;
    }

    this.entities.push(entity);
    return entity;
  }

  createCollection(name: string): void {
    if (Object.keys(this.collectionMatrix).includes(name)) {
      throw new Error(`Collection ${name} already exists`);
    }
    this.collectionMatrix[name] = {};
  }

  deleteCollection(name: string): void {
    if (!Object.keys(this.collectionMatrix).includes(name)) {
      throw new Error(`Collection ${name} does not exist`);
    }
    delete this.collectionMatrix[name];
  }

  getEntities(filters?: EntityFilter): Entity[] {
    if (!filters) {
      return this.entities;
    }

    let entities: Entity[] = this.entities;
    if (filters.ids !== undefined) {
      entities = entities.filter(e => (filters.ids as string[]).includes(e.id));
    }
    if (filters.collectionNames !== undefined) {
      entities = entities.filter((e) => {
        return (filters.collectionNames as string[])
          .map(name => this.collectionMatrix[name] !== undefined && this.collectionMatrix[name][e.id])
          .reduce((curr, agg) => curr || agg); // is in at least one of the collections
      });
    }
    if (filters.tags !== undefined) {
      entities = entities.filter((e) => {
        return (filters.tags as string[])
          .map(tag => this.tagMatrix[tag] !== undefined && this.tagMatrix[tag][e.id])
          .reduce((curr, agg) => curr || agg); // has at least one of the tags
      });
    }
    if (filters.components !== undefined) {
      entities = entities.filter((e) => {
        return (filters.components as {new(): IComponentData}[])
          .map(type => this.componentMatrix.get(type) !== undefined
            && (this.componentMatrix.get(type) as { [key: string]: boolean; })[e.id])
          .reduce((curr, agg) => curr && agg); // holds each of the components
      });
    }

    return entities;
  }

  addComponentToEntity<TComponentData extends IComponentData>(
    entity: Entity,
    componentDataConstructor: {new(): TComponentData},
    data?: TComponentData
  ): Component<TComponentData> {
    const component = ComponentFactory.build<TComponentData>(componentDataConstructor, data);
    entity.components.push(component);
    if (this.componentMatrix.get(componentDataConstructor) === undefined) {
      this.componentMatrix.set(componentDataConstructor, {});
    }
    (this.componentMatrix.get(componentDataConstructor) as { [key: string]: boolean; })[entity.id] = true;
    return component;
  }

  removeComponentFromEntity<TComponentData extends IComponentData>(
    entity: Entity,
    componentDataConstructor: {new(): TComponentData}
  ): void {
    entity.components = entity.components.filter(c => !(c.data instanceof componentDataConstructor));
    (this.componentMatrix.get(componentDataConstructor) as { [key: string]: boolean; })[entity.id] = false;
  }

  getComponentFromEntity<TComponentData extends IComponentData>(
    entity: Entity,
    componentDataConstructor: (new () => IComponentData)
  ): Component<TComponentData> {
    const matchingComponent = entity.components.find(c => c.data instanceof componentDataConstructor);
    if (matchingComponent === undefined) {
      throw new Error(`Invalid component asked for entity ${entity.id}`);
    }
    return matchingComponent as Component<TComponentData>;
  }

  destroyEntities(filters?: EntityFilter): void {
    const toDestroyIds = this.getEntities(filters).map(e => e.id);
    this.entities = this.entities.filter(e => !toDestroyIds.includes(e.id));
  }

  cleanup(): void {
    this.entities = [];
    this.componentMatrix = new Map<{new(): IComponentData}, { [key: string]: boolean; }>();
    this.collectionMatrix = {};
    this.tagMatrix = {};
  }
}

export class EntityFilter {
  collectionNames?: string[] = [];
  tags?: string[] = [];
  ids?: string[] = [];
  components?: (new () => IComponentData)[] = [];
}
