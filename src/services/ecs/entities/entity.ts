'use strict';

import Component, { IComponentData } from '../components/component';

export default class Entity {
  id: string;
  tags: string[];
  components: Component<IComponentData>[];
}
