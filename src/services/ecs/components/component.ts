'use strict';

export default class Component<TData extends IComponentData> {
  data: TData;
  state: any;
}

export interface IComponentData {}

export class ComponentFactory {
  static build<TData extends IComponentData>(
    componentStateConstructor: {new(): TData},
    data?: any
  ): Component<TData> {
    const component: Component<TData> = new Component<TData>();
    component.data = new componentStateConstructor();
    component.state = {};
    if (data) {
      Object.assign(component.data, data);
    }
    return component;
  }
}
