'use strict';

import Facade from '../../facade/facade';

export default abstract class SystemRoutine {
  constructor(
    protected scene: Phaser.Scene,
    protected F: Facade,
    protected options: any) {
  }

  startUp(): void {}
  apply(): void {}
}
