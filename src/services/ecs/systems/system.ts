'use strict';

import GameObject from '../../../game-objects/game-object';
import Component, { IComponentData } from '../components/component';
import Entity from '../entities/entity';

export default class System extends GameObject {
  protected get impactedEntities(): Entity[] {
    return [];
  }

  update(time: number, delta: number): void {}

  protected getComponent<TComponentData extends IComponentData>(
    entity: Entity,
    componentStateConstructor: {new(): TComponentData}
  ): Component<TComponentData> {
    return this.F.entityManager.getComponentFromEntity(entity, componentStateConstructor);
  }
}
