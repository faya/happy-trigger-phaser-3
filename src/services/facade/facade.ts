'use strict';

import IGlobalConfig from '../../config';
import EntityManager from '../ecs/entities/entity-manager';
import SubjectService from '../subject/subject-service';

export default class Facade {
  constructor(
    public entityManager: EntityManager,
    public subjectService: SubjectService,
    public config: IGlobalConfig
  ) {}
}
