# Happy trigger
## A phaser 3 html game written in typescript

#### The game can be played here :
https://faya.gitlab.io/happy-trigger-phaser-3/

Controls:
- arrow keys : move, jump
- mouse : aim
- esc : pause

#### To run locally with dev server :

```
> npm install
> npm start
```

#### Credits

code by **Nico F** (https://nfernand.github.io/)  
most spritesheets by **Bevouliin** (http://bevouliin.com)  
other assets from https://opengameart.org (CC0)

#### Licence
Licence for codebase : CC BY-NC-SA 2.0  
Licence for assets : CC0

